import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/page/request_fasilitas_form_page.dart';

class RequestFasilitasPage extends StatefulWidget {
  const RequestFasilitasPage({Key key}) : super(key: key);

  @override
  _RequestFasilitasPageController createState() =>
      _RequestFasilitasPageController();
}

class _RequestFasilitasPageController extends State<RequestFasilitasPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  List data;

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) => _RequestFasilitasPageView(this);

  void getData() async {
    setState(() => isLoading = true);

    try {
      Response response = await new Dio().post(
          API_URL + 'history_request_fasilitas',
          data: new FormData.fromMap({"id_user": await getSession("user_id")}),
          options: Options(method: 'POST', responseType: ResponseType.json));

      setState(() => isLoading = false);

      if (response.toString() != '') {
        setState(() {
          data = response.data['data'];
        });
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    }
  }

  Future<void> newRequest() async {
    await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => RequestFasilitasFormPage(),
        ));

    getData();
  }
}

class _RequestFasilitasPageView extends StatelessWidget {
  final _RequestFasilitasPageController state;

  const _RequestFasilitasPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: state.scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("Fasilitas"),
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () => state.newRequest(),
          label: Text('Fasilitas'),
          icon: Icon(Icons.add),
          backgroundColor: Colors.green,
        ),
        body: state.isLoading
            ? loadingScreen()
            : ListView.separated(
                itemCount: state.data.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(10),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                  state.data[index]['fasilitas'] +
                                      ' (' +
                                      state.data[index]['status'] +
                                      ')',
                                  style: TextStyle(fontSize: 16)),
                              SizedBox(height: 3),
                              Text(
                                  state.data[index]['request_start'] +
                                      ' - ' +
                                      state.data[index]['request_end'],
                                  style: TextStyle(color: Colors.black45))
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Text(state.data[index]['is_paid'],
                                  style: TextStyle(color: Colors.black45)),
                              SizedBox(height: 3),
                              Text(
                                  thousandSeparator(
                                          state.data[index]['biaya']) ??
                                      '',
                                  style: TextStyle(
                                      fontSize: 18, color: Colors.green))
                            ],
                          )
                        ]),
                  );
                },
                separatorBuilder: (context, index) {
                  return Divider();
                },
              ));
  }
}
