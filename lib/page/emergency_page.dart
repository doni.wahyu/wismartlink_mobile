import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';

class EmergencyPage extends StatefulWidget {
  const EmergencyPage({Key key}) : super(key: key);

  @override
  _EmergencyPageController createState() => _EmergencyPageController();
}

class _EmergencyPageController extends State<EmergencyPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  int counter = 0;
  bool isLoading = false;

  @override
  Widget build(BuildContext context) => _EmergencyPageView(this);

  void increaseCounter() => setState(() => counter += 1);

  void emergencyProcess() async {
    increaseCounter();

    if (counter == 5) {
      setState(() => isLoading = true);

      try {
        Response response = await new Dio().post(API_URL + 'emergency',
            data:
                new FormData.fromMap({"user_id": await getSession("user_id")}),
            options: Options(method: 'POST', responseType: ResponseType.json));

        setState(() => isLoading = false);

        if (response.toString() != '') {
          if (response.data['success'] == true) {
            showSnackBar(scaffoldKey, response.data['message']);
          } else {
            showSnackBar(scaffoldKey, response.data['message']);
          }
        } else {
          showSnackBar(scaffoldKey, NETWORK_ERROR);
        }
      } on DioError catch (e) {
        showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
      }
    }
  }
}

class _EmergencyPageView extends StatelessWidget {
  final _EmergencyPageController state;

  const _EmergencyPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: state.scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("Emergency"),
        ),
        body: state.isLoading
            ? loadingScreen()
            : Center(
                child: new SingleChildScrollView(
                  child: Container(
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.all(25),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          InkWell(
                              child: Text(
                            "Tekan 5 Kali",
                            textAlign: TextAlign.left,
                            style: TextStyle(fontSize: 25),
                          )),
                          InkWell(
                              child: Text(
                            "Untuk Memimta Pertolongan",
                            textAlign: TextAlign.left,
                            style: TextStyle(fontSize: 25),
                          )),
                          SizedBox(
                            height: 30.0,
                          ),
                          ClipOval(
                            child: Material(
                              color: Colors.red, // button color
                              child: InkWell(
                                splashColor: Colors.red, // inkwell color
                                child: SizedBox(
                                    width: 300,
                                    height: 300,
                                    child: Icon(
                                      Icons.warning,
                                      size: 170,
                                      color: Colors.white,
                                    )),
                                onTap: state.emergencyProcess,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 30.0,
                          ),
                          Text(
                            "${state.counter}",
                            style: TextStyle(fontSize: 30),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ));
  }
}
