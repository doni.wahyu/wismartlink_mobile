import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/page/marketplace/barang_update_page.dart';
import 'package:wismartlink/page/restaurant/makanan_register_page.dart';
import 'package:wismartlink/page/restaurant/makanan_update_page.dart';

class MakananPage extends StatefulWidget {
  const MakananPage({Key key}) : super(key: key);

  @override
  _MakananPageController createState() => _MakananPageController();
}

class _MakananPageController extends State<MakananPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  bool isPunyaToko = false;
  List data;
  TextEditingController searchController = new TextEditingController();

  @override
  void initState() {
    super.initState();

    getData();
  }

  @override
  Widget build(BuildContext context) => _BarangPageView(this);

  void getData() async {
    setState(() => isLoading = true);

    try {
      Response response = await new Dio().post(
          API_URL + 'list_makanan_by_resto_id',
          data: await new FormData.fromMap({
            "id_resto": await getSession("id_resto"),
            "keyword": searchController.text
          }),
          options: Options(method: 'POST', responseType: ResponseType.json));

      setState(() => isLoading = false);

      if (response.toString() != '') {
        setState(() {
          data = response.data['data'];
        });
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      print(e);
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    }
  }

  void awaitReturnRegisterBarang() async {
    final result = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => MakananRegisterPage(),
        ));

    getData();
  }
}

class _BarangPageView extends StatelessWidget {
  final _MakananPageController state;

  const _BarangPageView(this.state, {Key key}) : super(key: key);

  _delteConfirmation(makananId) {
    showDialog(
      context: state.scaffoldKey.currentContext,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Delete Makanan",
              style: new TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 14.0)),
          content: SingleChildScrollView(
            child: Container(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Are You Sure ? ",
                      style: TextStyle(color: Colors.black),
                    )
                  ]),
            ),
          ),
          actions: <Widget>[
            new FlatButton(
              color: Colors.green,
              child: new Text(
                "Delete",
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.pop(context);
                _deleteBarang(makananId);
              },
            ),
            // usually buttons at the bottom of the dialog
            new FlatButton(
              color: Colors.red,
              child: new Text(
                "Cancel",
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                //UPDATE TRACK KE 4.1 YAITU PENGAJUAN DILAKUKAN SURVEY EKSTERNAL
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  _deleteBarang(makananId) async {
    var param = {
      "id": makananId,
    };
    print('delete ' + param.toString());
    // print('_selectedDestination ' + _selectedDestination.toString());
    final url = API_URL;
    var dio = new Dio();
    try {
      Response response;
      response = await dio.post(url + "disable_makanan_resto",
          data: param,
          options: new Options(
            headers: {"Content-Type": "application/x-www-form-urlencoded"},
          ));

      print(response.data.toString());
      print(response.data['success']);
      if (response.data['success'] == true) {
        Alert(
          context: state.scaffoldKey.currentContext,
          type: AlertType.success,
          title: "Delete Makanan",
          desc: response.data['message'],
          buttons: [
            DialogButton(
              child: Text(
                "OK",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () {
                Navigator.pop(state.scaffoldKey.currentContext);
                Navigator.of(state.scaffoldKey.currentContext)
                    .pushReplacement(new MaterialPageRoute(builder: (_) {
                  return new MakananPage();
                }));
              },
              width: 120,
            )
          ],
        ).show();
      } else {
        Alert(
          context: state.scaffoldKey.currentContext,
          type: AlertType.error,
          title: "Delete Makanan",
          desc: response.data['message'],
          buttons: [
            DialogButton(
              child: Text(
                "OK",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () {
                Navigator.pop(state.scaffoldKey.currentContext);
              },
              width: 120,
            )
          ],
        ).show();
      }
    } on DioError catch (e) {
      debugPrint("Error $e");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: state.scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(children: <Widget>[Text("Makanan")]),
              Column(children: <Widget>[
                InkWell(
                    onTap: () {
                      Navigator.push(
                          state.context,
                          MaterialPageRoute(
//                            builder: (context) => NotifikasiPage(),
                              ));
                    },
                    child: Icon(
                      Icons.shopping_cart,
                      color: Colors.white,
                    ))
              ])
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () => state.awaitReturnRegisterBarang(),
          label: Text('Tambah Makanan'),
          icon: Icon(Icons.add_to_photos),
          backgroundColor: Colors.green,
        ),
        body: new Column(
          children: <Widget>[
            new Container(
              color: hexToColor("#344b6b"),
              child: new Padding(
                padding: const EdgeInsets.all(3),
                child: new Card(
                  child: new ListTile(
                    leading: new Icon(Icons.search),
                    title: new TextField(
                      controller: state.searchController,
                      decoration: new InputDecoration(
                          hintText: 'Search', border: InputBorder.none),
                      onChanged: (text) {
                        state.getData();
                      },
                    ),
                    trailing: new IconButton(
                      icon: new Icon(Icons.cancel),
                      onPressed: () {
                        state.searchController.clear();
                        state.getData();
                      },
                    ),
                  ),
                ),
              ),
            ),
            new Expanded(
                child: state.isLoading
                    ? loadingScreen()
                    : ListView.separated(
                        itemCount: state.data == null ? 0 : state.data.length,
                        itemBuilder: (context, index) {
                          return Row(
                            children: <Widget>[
                              Expanded(
                                  flex: 2,
                                  child: Image.network(
                                    state.data[index]['img'],
                                    width: 80.0,
                                    height: 80.0,
                                  )),
                              Expanded(
                                flex: 2,
                                child: Column(
                                  children: <Widget>[
                                    Align(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                            state.data[index]['nama_makanan'])),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(thousandSeparator(
                                          state.data[index]['harga'])),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: InkWell(
                                    onTap: () {
                                      Navigator.of(context).pushReplacement(
                                          new MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            new MakananUpdatePage(
                                                state.data[index]['id'],
                                                state.data[index]
                                                    ['nama_makanan'],
                                                state.data[index]['harga'],
                                                state.data[index]['img']),
                                      ));
                                    },
                                    child: Icon(Icons.edit)),
                              ),
                              Expanded(
                                flex: 2,
                                child: InkWell(
                                  onTap: () {
                                    _delteConfirmation(state.data[index]['id']);
                                    // _deleteDestination(dest[i].destinationId);
                                  },
                                  child: Icon(
                                    Icons.delete,
                                    color: Colors.black,
                                    size: 20.0,
                                  ),
                                ),
                              )
                            ],
                          );
                          // return ListTile(
                          //   leading: Image.network(state.data[index]['img']),
                          //   title: Text(state.data[index]['nama_barang']),
                          //   subtitle: Text(
                          //       thousandSeparator(state.data[index]['harga'])),
                          // );
                        },
                        separatorBuilder: (context, index) {
                          return Divider();
                        },
                      )),
          ],
        ));
  }
}
