import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/page/login_page.dart';
import 'package:wismartlink/page/profile_page.dart';

import 'generic_info_page.dart';
import 'home_page.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key key}) : super(key: key);

  @override
  _MainPageController createState() => _MainPageController();
}

class _MainPageController extends State<MainPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  int selectedBottomNavigation = 0;

  final bottomNavigationPage = [
    HomePage(),
    ProfilePage()
  ];

  @override
  void initState() {
    super.initState();


  }

  void _onItemTapped(int index) {
    setState(() {
      selectedBottomNavigation = index;
    });
  }

  @override
  Widget build(BuildContext context) => _MainPageView(this);
}

class _MainPageView extends StatelessWidget {
  final _MainPageController state;

  const _MainPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: state.bottomNavigationPage.elementAt(state.selectedBottomNavigation),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            title: Text('Profile'),
          ),
        ],
        type: BottomNavigationBarType.fixed,
        currentIndex: state.selectedBottomNavigation,
        fixedColor: hexToColor("#344b6b"),
        onTap: state._onItemTapped,
      ),
    );
  }

}
