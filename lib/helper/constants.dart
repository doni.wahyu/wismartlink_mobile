enum AlertAction {
  cancel,
  discard,
  disagree,
  agree,
}

const String API_URL = "";
const String NETWORK_ERROR = "Network Error";
const String SERVER_RESPONSE_ERROR = "Server Response Error";
const bool devMode = false;
const double textScaleFactor = 1.0;
