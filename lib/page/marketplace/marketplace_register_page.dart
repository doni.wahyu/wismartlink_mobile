import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';

class MarketPlaceRegisterPage extends StatefulWidget {
  const MarketPlaceRegisterPage({Key key}) : super(key: key);

  @override
  _MarketPlaceRegisterPageController createState() =>
      _MarketPlaceRegisterPageController();
}

class _MarketPlaceRegisterPageController
    extends State<MarketPlaceRegisterPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  File imageFile;
  dynamic pickImageError;
  String retrieveDataError;
  TextEditingController namaTokoController = new TextEditingController();

  @override
  Widget build(BuildContext context) => _MarketPlaceRegisterPageView(this);

  void registerToko() async {
    if (namaTokoController.text.isEmpty) {
      showSnackBar(scaffoldKey, "Nama toko harap diisi");
    } else if (imageFile == null) {
      showSnackBar(scaffoldKey, "Gambar Toko belum dipilih");
    } else {
      setState(() => isLoading = true);

      try {
        Response response = await new Dio().post(API_URL + 'register_toko',
            data: new FormData.fromMap({
              "id_user": await getSession("user_id"),
              "nama_toko": namaTokoController.text,
              "img": await MultipartFile.fromFile(imageFile.path,
                  filename: imageFile.path.split('/').last),
            }),
            options: Options(method: 'POST', responseType: ResponseType.json));

        setState(() => isLoading = false);

        if (response.toString() != '') {
          if (response.data['success'] == true) {
            Alert(
              context: context,
              type: AlertType.success,
              title: response.data['message'],
              buttons: [
                DialogButton(
                  child: Text(
                    "OK",
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                  onPressed: () async {
                    putSession("id_toko", response.data['id_toko']);

                    Navigator.pop(context);
                    Navigator.pop(context);
                  },
                  width: 120,
                )
              ],
            ).show();
          } else {
            showSnackBar(scaffoldKey, response.data['message']);
          }
        } else {
          showSnackBar(scaffoldKey, NETWORK_ERROR);
        }
      } on DioError catch (e) {
        showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
      }
    }
  }

  void chooseImage(ImageSource source) async {
    try {
      imageFile = await ImagePicker.pickImage(source: source, imageQuality: 50);
      setState(() {});
    } catch (e) {
      pickImageError = e;
    }
  }

  Widget _previewImage() {
    final Text retrieveError = _getRetrieveErrorWidget();
    if (retrieveError != null) {
      return retrieveError;
    }
    if (imageFile != null) {
      return ClipOval(
          child: Image.file(
        imageFile,
        width: 200,
        height: 200,
      ));
    } else if (pickImageError != null) {
      return Text(
        'Pick image error: $pickImageError',
        textAlign: TextAlign.center,
      );
    } else {
      return const Text(
        'Gambar Toko',
        textAlign: TextAlign.center,
      );
    }
  }

  Text _getRetrieveErrorWidget() {
    if (retrieveDataError != null) {
      final Text result = Text(retrieveDataError);
      retrieveDataError = null;
      return result;
    }
    return null;
  }

  Future<void> retrieveLostData() async {
    final LostDataResponse response = await ImagePicker.retrieveLostData();
    if (response.isEmpty) {
      return;
    }
    if (response.file != null) {
      setState(() {
        imageFile = response.file;
      });
    } else {
      retrieveDataError = response.exception.code;
    }
  }
}

class _MarketPlaceRegisterPageView extends StatelessWidget {
  final _MarketPlaceRegisterPageController state;

  const _MarketPlaceRegisterPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: state.scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("Buka Toko"),
        ),
        body: state.isLoading
            ? loadingScreen()
            : Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      TextField(
                        controller: state.namaTokoController,
                        obscureText: false,
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            hintText: "Nama Toko",
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(32.0))),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(5),
                            child: FloatingActionButton(
                              onPressed: () {
                                state.chooseImage(ImageSource.gallery);
                              },
                              heroTag: 'image0',
                              child: const Icon(Icons.photo_library),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(5),
                            child: FloatingActionButton(
                              onPressed: () {
                                state.chooseImage(ImageSource.camera);
                              },
                              heroTag: 'image1',
                              child: const Icon(Icons.photo_camera),
                            ),
                          ),
                          Platform.isAndroid
                              ? FutureBuilder<void>(
                                  future: state.retrieveLostData(),
                                  builder: (BuildContext context,
                                      AsyncSnapshot<void> snapshot) {
                                    switch (snapshot.connectionState) {
                                      case ConnectionState.none:
                                      case ConnectionState.waiting:
                                        return const Text(
                                          'Gambar belum dipilih.',
                                          textAlign: TextAlign.center,
                                        );
                                      case ConnectionState.done:
                                        return state._previewImage();
                                      default:
                                        if (snapshot.hasError) {
                                          return Text(
                                            'Pick image/video error: ${snapshot.error}}',
                                            textAlign: TextAlign.center,
                                          );
                                        } else {
                                          return const Text(
                                            'Gambar belum dipilih.',
                                            textAlign: TextAlign.center,
                                          );
                                        }
                                    }
                                  },
                                )
                              : state._previewImage()
                        ],
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Material(
                        elevation: 5.0,
                        borderRadius: BorderRadius.circular(30.0),
                        color: Colors.green,
                        child: MaterialButton(
                          minWidth:
                              MediaQuery.of(this.state.context).size.width,
                          padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                          onPressed: state.registerToko,
                          child: Text(
                            "SUBMIT",
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ));
  }
}

typedef void OnPickImageCallback(
    double maxWidth, double maxHeight, int quality);
