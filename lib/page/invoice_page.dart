import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';

class InvoicePage extends StatefulWidget {
  const InvoicePage({Key key}) : super(key: key);

  @override
  _InvoicePageController createState() => _InvoicePageController();
}

class _InvoicePageController extends State<InvoicePage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  List data;

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) => _InvoicePageView(this);

  void getData() async {
    setState(() => isLoading = true);

    try {
      Response response = await new Dio().post(API_URL + 'history_invoice',
          data: new FormData.fromMap({"id_user": await getSession("user_id")}),
          options: Options(method: 'POST', responseType: ResponseType.json));

      setState(() => isLoading = false);

      if (response.toString() != '') {
        setState(() {
          data = response.data['data'];
        });
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    }
  }
}

class _InvoicePageView extends StatelessWidget {
  final _InvoicePageController state;

  const _InvoicePageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: state.scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("Invoice"),
        ),
        body: state.isLoading
            ? loadingScreen()
            : ListView.separated(
                itemCount: state.data.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(10),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(state.data[index]['no_invoice'],
                                  style: TextStyle(fontSize: 16)),
                              SizedBox(height: 3),
                              Text(state.data[index]['invoice_date'],
                                  style: TextStyle(color: Colors.black45))
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Text(state.data[index]['is_paid'],
                                  style: TextStyle(color: Colors.black45)),
                              SizedBox(height: 3),
                              Text(state.data[index]['total'] ?? '',
                                  style: TextStyle(
                                      fontSize: 18, color: Colors.green))
                            ],
                          )
                        ]),
                  );
                },
                separatorBuilder: (context, index) {
                  return Divider();
                },
              ));
  }
}
