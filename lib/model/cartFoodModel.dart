// To parse this JSON data, do
//
//     final cartModel = cartModelFromJson(jsonString);

import 'dart:convert';

CartFoodModel cartModelFromJson(String str) => CartFoodModel.fromJson(json.decode(str));

String cartModelToJson(CartFoodModel data) => json.encode(data.toJson());

class CartFoodModel {
  CartFoodModel({
    this.id,
    this.namaMakanan,
    this.hargaPerPorsi,
    this.qty,
    this.totalHarga,
    this.subtotal,
    this.img,
  });

  String id;
  String namaMakanan;
  String hargaPerPorsi;
  String qty;
  String totalHarga;
  String subtotal;
  String img;

  factory CartFoodModel.fromJson(Map<String, dynamic> json) => CartFoodModel(
        id: json["id"],
        namaMakanan: json["nama_makanan"],
        hargaPerPorsi: json["harga_per_porsi"],
        qty: json["qty"],
        totalHarga: json["total_harga"],
        subtotal: json["subtotal"],
        img: json["img"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nama_makanan": namaMakanan,
        "harga_per_porsi": hargaPerPorsi,
        "qty": qty,
        "total_harga": totalHarga,
        "subtotal": subtotal,
        "img": img,
      };
}
