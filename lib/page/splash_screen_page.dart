import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/page/login_page.dart';
import 'package:wismartlink/page/main_page.dart';

class SplashScreenPage extends StatefulWidget {
  const SplashScreenPage({Key key}) : super(key: key);

  @override
  _SplashScreenPageController createState() => _SplashScreenPageController();
}

class _SplashScreenPageController extends State<SplashScreenPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();

    validateSession();
  }

  @override
  Widget build(BuildContext context) => _SplashScreenPageView(this);

  validateSession() async {
    String user_id = await getSession("user_id");
    print(user_id);
    if (user_id == null) {
      loginPage();
    } else {
      mainPage();
    }
  }

  loginPage() {
    var duration = const Duration(seconds: 1);
    new Timer(duration, () {
      Timer(duration, () {
        Navigator.pushReplacement(context, SlideRightRoute(page: LoginPage()));
      });
    });
  }

  mainPage() {
    var duration = const Duration(seconds: 1);
    Timer(duration, () {
      Navigator.pushReplacement(context, SlideRightRoute(page: MainPage()));
    });
  }
}

class _SplashScreenPageView extends StatelessWidget {
  final _SplashScreenPageController state;

  const _SplashScreenPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 150.0,
              child: Image.asset(
                "assets/images/wismartlogo.png",
                fit: BoxFit.contain,
              ),
            )
          ]),
      decoration: new BoxDecoration(
          image: new DecorationImage(
              image: new ExactAssetImage('assets/images/bglogin.jpg'),
              fit: BoxFit.cover)),
    );
  }
}
