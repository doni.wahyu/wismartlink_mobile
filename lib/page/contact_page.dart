import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';

class ContactPage extends StatefulWidget {
  const ContactPage({Key key}) : super(key: key);

  @override
  _ContactPageController createState() => _ContactPageController();
}

class _ContactPageController extends State<ContactPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  List data;

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) => _ContactPageView(this);

  void getData() async {
    setState(() => isLoading = true);

    try {
      Response response = await new Dio().post(API_URL + 'data_penghuni',
          options: Options(method: 'POST', responseType: ResponseType.json));

      setState(() => isLoading = false);

      if (response.toString() != '') {
        setState(() {
          data = response.data['data'];
        });
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    }
  }
}

class _ContactPageView extends StatelessWidget {
  final _ContactPageController state;

  const _ContactPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: state.scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("Contact"),
        ),
        body: state.isLoading
            ? loadingScreen()
            : ListView.separated(
                itemCount: state.data.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text(state.data[index]['nama']),
                    subtitle: Text(state.data[index]['phone_number']),
                  );
                  ;
                },
                separatorBuilder: (context, index) {
                  return Divider();
                },
              ));
  }
}
