import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/page/chat_page.dart';
import 'package:wismartlink/page/emergency_page.dart';
import 'package:wismartlink/page/generic_info_page.dart';
import 'package:wismartlink/page/marketplace/marketplace_page.dart';
import 'package:wismartlink/page/notifikasi_page.dart';
import 'package:wismartlink/page/request_fasilitas_page.dart';
import 'package:wismartlink/page/request_maintenance_page.dart';
import 'package:wismartlink/page/request_roomservice_page.dart';
import 'package:wismartlink/page/restaurant/restaurant_page.dart';

import 'cctv_page.dart';
import 'contact_page.dart';
import 'invoice_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageController createState() => _HomePageController();
}

class _HomePageController extends State<HomePage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  TextEditingController requestController = new TextEditingController();
  TextEditingController requestDateController = new TextEditingController();

  final List<String> imgList = [
    'assets/images/slide1.png',
    'assets/images/slide2.png',
    'assets/images/slide3.jpg',
    'assets/images/slide4.png',
    'assets/images/slide5.png',
    'assets/images/slide6.png',
    'assets/images/slide7.png',
  ];

  final List<Menu> menus = const [
    const Menu(title: 'Emergency', icon: 'assets/icon/scurity.png'),
    const Menu(title: 'CCTV', icon: 'assets/icon/cctv.png'),
    const Menu(title: 'Informasi', icon: 'assets/icon/Informasi  atau Pengumuman.png'),
    const Menu(title: 'Maintenance', icon: 'assets/icon/manitence.png'),
    const Menu(title: 'Fasilitas', icon: 'assets/icon/fasilitas.png'),
    const Menu(title: 'Invoice', icon: 'assets/icon/billing invoice.png'),
    const Menu(title: 'Contact', icon: 'assets/icon/kontak.png'),
    const Menu(title: 'Room Service', icon: 'assets/icon/room service.png'),
    const Menu(title: 'Market Place', icon: 'assets/icon/toko.png'),
    const Menu(title: 'Restaurant', icon: 'assets/icon/Restauran.png'),
    const Menu(title: 'Chat', icon: 'assets/icon/inbox.png'),
    const Menu(title: 'Helpdesk', icon: 'assets/icon/helpdesk.png'),
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) => _HomePageView(this);
}

class _HomePageView extends StatelessWidget {
  final _HomePageController state;

  const _HomePageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: state.scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(children: <Widget>[
                Image.asset(
                  'assets/images/wismartlogosmall.png',
                  fit: BoxFit.contain,
                  height: 25,
                )
              ]),
              Column(children: <Widget>[
                InkWell(
                    onTap: () {
                      Navigator.push(
                          state.context,
                          MaterialPageRoute(
                            builder: (context) => NotifikasiPage(),
                          ));
                    },
                    child: Icon(
                      Icons.notifications,
                      color: Colors.white,
                    ))
              ])
            ],
          ),
        ),
        body: state.isLoading
            ? loadingScreen()
            : SingleChildScrollView(
                child: Container(
                  color: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        CarouselSlider(
                          height: 150,
                          autoPlay: true,
                          enableInfiniteScroll: true,
                          viewportFraction: 1.0,
                          enlargeCenterPage: true,
                          aspectRatio:
                              MediaQuery.of(state.context).size.aspectRatio,
                          items: state.imgList.map(
                            (url) {
                              return Container(
                                child: ClipRRect(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(0.0)),
                                  child: Image(image: AssetImage(url)),
                                ),
                              );
                            },
                          ).toList(),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        new Container(
                          height: 400,
                          child: GridView.count(
                              crossAxisCount: 4,
                              children:
                                  List.generate(state.menus.length, (index) {
                                return InkWell(
                                    onTap: () {
                                      getGridViewSelectedItem(
                                          state.menus[index]);
                                    },
                                    child: Center(
                                      child: MenuCard(menu: state.menus[index]),
                                    ));
                              })),
                        ),
                      ],
                    ),
                  ),
                ),
              ));
  }

  getGridViewSelectedItem(Menu menu) {
    StatefulWidget widget;
    if (menu.title == "Emergency") {
      widget = EmergencyPage();
    } else if (menu.title == "CCTV") {
      widget = CctvPage();
    } else if (menu.title == "Informasi") {
      widget = GenericInfoPage();
    } else if (menu.title == "Maintenance") {
      widget = RequestMaintenancePage();
    } else if (menu.title == "Fasilitas") {
      widget = RequestFasilitasPage();
    } else if (menu.title == "Invoice") {
      widget = InvoicePage();
    } else if (menu.title == "Contact") {
      widget = ContactPage();
    } else if (menu.title == "Room Service") {
      widget = RequestRoomServicePage();
    } else if (menu.title == "Market Place") {
      widget = MarketPlacePage();
    } else if (menu.title == "Restaurant") {
      widget = RestaurantPage();
    } else if (menu.title == "Chat") {
      widget = ChatPage();
    } else if (menu.title == "Helpdesk") {
      widget = ChatPage();
    }

    Navigator.push(
        state.context,
        MaterialPageRoute(
          builder: (context) => widget,
        ));
  }
}

class Menu {
  const Menu({this.title, this.icon});

  final String title;
  final String icon;
}

class MenuCard extends StatelessWidget {
  const MenuCard({Key key, this.menu}) : super(key: key);

  final Menu menu;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Center(
        child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.all(5),
                child: Image.asset(
                  menu.icon,
                  fit: BoxFit.contain,
                  height: 10,
                ),
              )),
              SizedBox(
                height: 3.0,
              ),
              Text(menu.title, style: TextStyle(fontSize: 12)),
            ]),
      ),
    );
  }
}
