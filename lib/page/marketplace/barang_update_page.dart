// import 'package:dio/dio.dart';
import 'package:path/path.dart' as path;
import 'package:async/async.dart';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:toast/toast.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';

class BarangUpdatePage extends StatefulWidget {
  const BarangUpdatePage(
      this.id, this.nama_barang, this.harga, this.keterangan, this.img);
  final String id;
  final String nama_barang;
  final String harga;
  final String keterangan;
  final String img;

  @override
  _BarangUpdatePageController createState() => _BarangUpdatePageController();
}

class _BarangUpdatePageController extends State<BarangUpdatePage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  var id = '';
  final _formKey = GlobalKey<FormState>();
  // you must keep track of the TextEditingControllers if you want the values to persist correctly
  List<TextEditingController> controllers = <TextEditingController>[];
  TextEditingController namaBarangController = TextEditingController();
  TextEditingController hargaController = TextEditingController();
  TextEditingController keteranganController = TextEditingController();
  bool isLoading = false;
  dynamic pickImageError;

  String retrieveDataError;
  File _imageFile;

  @override
  void initState() {
    super.initState(); 
    namaBarangController.text = widget.nama_barang;
    hargaController.text = widget.harga;
    keteranganController.text = widget.keterangan;
    print('widget.keterangan ' + keteranganController.text);
  }

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: Toast.LENGTH_LONG, gravity: gravity);
  }

  void registerBarang() async {
    if (namaBarangController.text.isEmpty) {
      showSnackBar(scaffoldKey, "Nama barang harap diisi");
    } else if (hargaController.text.isEmpty) {
      showSnackBar(scaffoldKey, "Harga harap diisi");
    } else if (_imageFile == null) {
      setState(() => isLoading = true);
      try {
        var uri = Uri.parse(API_URL + 'update_barang_toko');
        var request = http.MultipartRequest("POST", uri);
        request.fields['nama_barang'] = namaBarangController.text;
        request.fields['harga'] = hargaController.text;
        request.fields['keterangan'] = keteranganController.text;
        request.fields['id'] = widget.id;
        request.fields['id_toko'] = await getSession("id_toko");

        var response = await request.send();
        if (response.toString() != '') {
          if (response.statusCode == 200) {
            Alert(
              context: context,
              type: AlertType.success,
              title: 'success',
              buttons: [
                DialogButton(
                  child: Text(
                    "OK",
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                  onPressed: () async {
                    Navigator.pop(context);
                    Navigator.pop(context);
                  },
                  width: 120,
                )
              ],
            ).show();
          } else {
            showSnackBar(scaffoldKey, 'failed');
          }
        } else {
          showSnackBar(scaffoldKey, NETWORK_ERROR);
        }
      } catch (e) {
        debugPrint("Error $e");
      }
    } else {
      setState(() => isLoading = true);

      try {
        var stream =
            http.ByteStream(DelegatingStream.typed(_imageFile.openRead()));
        var length = await _imageFile.length();
        var uri = Uri.parse(API_URL + 'update_barang_toko');
        var request = http.MultipartRequest("POST", uri);
        request.fields['nama_barang'] = namaBarangController.text;
        request.fields['harga'] = hargaController.text; 
        request.fields['id_toko'] = await getSession("id_toko");
        request.fields['id'] = widget.id;

        request.files.add(http.MultipartFile("img", stream, length,
            filename: path.basename(_imageFile.path)));
        var response = await request.send();
        if (response.toString() != '') {
          if (response.statusCode == 200) {
            Alert(
              context: context,
              type: AlertType.success,
              title: 'success',
              buttons: [
                DialogButton(
                  child: Text(
                    "OK",
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                  onPressed: () async {
                    Navigator.pop(context);
                    Navigator.pop(context);
                  },
                  width: 120,
                )
              ],
            ).show();
          } else {
            showSnackBar(scaffoldKey, 'failed');
          }
        } else {
          showSnackBar(scaffoldKey, NETWORK_ERROR);
        }
      } catch (e) {
        debugPrint("Error $e");
      }
    }
  }

  void chooseImage(ImageSource source) async {
    try {
      _imageFile =
          await ImagePicker.pickImage(source: source, imageQuality: 50);
      setState(() {});
    } catch (e) {
      pickImageError = e;
    }
  }

  _selectImage(ImageSource source) async {
    var image = await ImagePicker.pickImage(
        source: source, maxHeight: 1920.0, maxWidth: 1080.0);
    setState(() {
      _imageFile = image;
    });
  }

  Widget _previewImage() {
    final Text retrieveError = _getRetrieveErrorWidget();
    if (retrieveError != null) {
      return retrieveError;
    }
    if (_imageFile != null) {
      return ClipOval(
          child: Image.file(
        _imageFile,
        width: 200,
        height: 200,
      ));
    } else if (pickImageError != null) {
      return Text(
        'Pick image error: $pickImageError',
        textAlign: TextAlign.center,
      );
    } else {
      return Image.network(
        widget.img,
        width: 80.0,
        height: 80.0,
      );
    }
  }

  Text _getRetrieveErrorWidget() {
    if (retrieveDataError != null) {
      final Text result = Text(retrieveDataError);
      retrieveDataError = null;
      return result;
    }
    return null;
  }

  Future<void> retrieveLostData() async {
    final LostDataResponse response = await ImagePicker.retrieveLostData();
    if (response.isEmpty) {
      return;
    }
    if (response.file != null) {
      setState(() {
        _imageFile = response.file;
      });
    } else {
      retrieveDataError = response.exception.code;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _formKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("Barang"),
        ),
        body: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  TextField(
                    controller: namaBarangController,
                    decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                        hintText: "Nama Barang",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(32.0))),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextField(
                    controller: hargaController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                        hintText: "Harga",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(32.0))),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextField(
                    maxLines: 8,
                    controller: keteranganController,
                    decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                        hintText: "Keterangan",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(32.0))),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(5),
                        child: FloatingActionButton(
                          onPressed: () {
                            _selectImage(ImageSource.gallery);
                          },
                          heroTag: 'image0',
                          child: const Icon(Icons.photo_library),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5),
                        child: FloatingActionButton(
                          onPressed: () {
                            _selectImage(ImageSource.camera);
                          },
                          heroTag: 'image1',
                          child: const Icon(Icons.photo_camera),
                        ),
                      ),
                      Platform.isAndroid
                          ? FutureBuilder<void>(
                              future: retrieveLostData(),
                              builder: (BuildContext context,
                                  AsyncSnapshot<void> snapshot) {
                                switch (snapshot.connectionState) {
                                  case ConnectionState.none:
                                  case ConnectionState.waiting:
                                    return const Text(
                                      'Gambar belum dipilih.',
                                      textAlign: TextAlign.center,
                                    );
                                  case ConnectionState.done:
                                    return _previewImage();
                                  default:
                                    if (snapshot.hasError) {
                                      return Text(
                                        'Pick image/video error: ${snapshot.error}}',
                                        textAlign: TextAlign.center,
                                      );
                                    } else {
                                      return const Text(
                                        'Gambar belum dipilih.',
                                        textAlign: TextAlign.center,
                                      );
                                    }
                                }
                              },
                            )
                          : _previewImage()
                    ],
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(30.0),
                    color: Colors.green,
                    child: MaterialButton(
                      minWidth: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      onPressed: registerBarang,
                      child: Text(
                        "SUBMIT",
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
