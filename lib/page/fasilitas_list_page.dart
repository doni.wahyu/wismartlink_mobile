import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';

class FasilitasListPage extends StatefulWidget {
  const FasilitasListPage({Key key}) : super(key: key);

  @override
  _FasilitasListPageController createState() => _FasilitasListPageController();
}

class _FasilitasListPageController extends State<FasilitasListPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  List data;

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) => _FasilitasListPageView(this);

  void getData() async {
    setState(() => isLoading = true);

    try {
      Response response = await new Dio().post(API_URL + 'fasilitas',
          options: Options(method: 'POST', responseType: ResponseType.json));

      setState(() => isLoading = false);

      if (response.toString() != '') {
        setState(() {
          data = response.data['data'];
        });
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    }
  }
}

class _FasilitasListPageView extends StatelessWidget {
  final _FasilitasListPageController state;

  const _FasilitasListPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: state.scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("Fasilitas"),
        ),
        body: state.isLoading
            ? loadingScreen()
            : ListView.separated(
                itemCount: state.data.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text(state.data[index]['fasilitas']),
                    onTap: () {
                      Navigator.pop(context, state.data[index]);
                    },
                  );
                },
                separatorBuilder: (context, index) {
                  return Divider();
                },
              ));
  }
}
