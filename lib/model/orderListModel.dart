// To parse this JSON data, do
//
//     final orderListModel = orderListModelFromJson(jsonString);

import 'dart:convert';

OrderListModel orderListModelFromJson(String str) =>
    OrderListModel.fromJson(json.decode(str));

String orderListModelToJson(OrderListModel data) => json.encode(data.toJson());

class OrderListModel {
  OrderListModel({
    this.id,
    this.kodeTransaksi,
    this.status,
    this.statusOrder,
    this.grandTotal,
    this.listOrder,
  });

  String id;
  String kodeTransaksi;
  String status;
  String statusOrder;
  String grandTotal;
  List<ListOrder> listOrder;

  factory OrderListModel.fromJson(Map<String, dynamic> json) => OrderListModel(
        id: json["id"],
        kodeTransaksi: json["kode_transaksi"],
        status: json["status"],
        statusOrder: json["status_order"],
        grandTotal: json["grand_total"],
        listOrder: List<ListOrder>.from(
            json["list_order"].map((x) => ListOrder.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "kode_transaksi": kodeTransaksi,
        "status_order": statusOrder,
        "grand_total": grandTotal,
        "list_order": List<dynamic>.from(listOrder.map((x) => x.toJson())),
      };
}

class ListOrder {
  ListOrder({
    this.idBarang,
    this.jumlahBarang,
    this.namaBarang,
    this.img,
  });

  String idBarang;
  String jumlahBarang;
  String namaBarang;
  String img;

  factory ListOrder.fromJson(Map<String, dynamic> json) => ListOrder(
        idBarang: json["id_barang"],
        jumlahBarang: json["jumlah_barang"],
        namaBarang: json["nama_barang"],
        img: json["img"],
      );

  Map<String, dynamic> toJson() => {
        "id_barang": idBarang,
        "jumlah_barang": jumlahBarang,
        "nama_barang": namaBarang,
        "img": img,
      };
}
