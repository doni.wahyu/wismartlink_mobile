-- --------------------------------------------------------
-- Host:                         febuharyanto.com
-- Server version:               10.2.31-MariaDB - MariaDB Server
-- Server OS:                    Linux
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table u6255547_apartemen.admin
CREATE TABLE IF NOT EXISTS `admin` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `phone_number` varchar(100) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `img` text DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table u6255547_apartemen.admin: 12 rows
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT IGNORE INTO `admin` (`user_id`, `nama`, `username`, `email`, `password`, `phone_number`, `level`, `status`, `img`, `is_deleted`) VALUES
	(1, 'Super Admin', 'superadmin', 'superadmin@gmail.com', 'n3uhp66XeKSl', NULL, 1, 1, NULL, 0),
	(4, 'admin', 'admin', 'admin@gmail.com', 'n3uhp66XeKSl', NULL, 2, 1, NULL, 0),
	(5, 'General Manager', 'generalmanager', 'generalmanager@gmail.com', 'n3uhp66XeKSl', NULL, 3, 1, NULL, 0),
	(6, 'Finance Manager', 'financemanager', 'financemanager@gmail.com', NULL, '081381612011', 4, 1, NULL, 0),
	(7, 'Security 1', 'security1', 'security1@gmail.com', NULL, '081381612011', 15, 1, NULL, 0),
	(8, 'Security 2', 'security2', 'security2@gmail.com', NULL, '081381612011', 15, 1, NULL, 0),
	(9, 'Support1', 'support1', 'support1@gmail.com', NULL, '081381612011', 14, 1, NULL, 0),
	(10, 'Support2', 'support2', 'support2@gmail.com', NULL, '081381612011', 14, 1, NULL, 0),
	(11, 'Cleaning1', 'cleaning1', 'cleaning1@gmail.com', NULL, '081381612011', 10, 1, NULL, 0),
	(12, 'Cleaning2', 'cleaning2', 'cleaning2@gmail.com', NULL, '081381612011', 10, 1, NULL, 0),
	(13, 'Maintenance1', 'maintenance1', 'maintenance1@gmail.com', NULL, '081381612011', 8, 1, NULL, 0),
	(14, 'Maintenance2', 'maintenance2', 'maintenance2@gmail.com', NULL, '081381612011', 8, 1, NULL, 0);
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.apartemen
CREATE TABLE IF NOT EXISTS `apartemen` (
  `id_apt` int(11) NOT NULL AUTO_INCREMENT,
  `nama_apt` varchar(255) DEFAULT NULL,
  `kode_apt` varchar(10) DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  PRIMARY KEY (`id_apt`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.apartemen: 3 rows
/*!40000 ALTER TABLE `apartemen` DISABLE KEYS */;
INSERT IGNORE INTO `apartemen` (`id_apt`, `nama_apt`, `kode_apt`, `is_deleted`) VALUES
	(1, 'Kalibata City', '001', 0),
	(2, 'Taman Anggrek Residences', '002', 0),
	(3, 'Puri Park View', '003', 0);
/*!40000 ALTER TABLE `apartemen` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.barang_toko
CREATE TABLE IF NOT EXISTS `barang_toko` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_toko` int(11) NOT NULL DEFAULT 0,
  `nama_barang` varchar(50) DEFAULT NULL,
  `keterangan` text NOT NULL DEFAULT '',
  `harga` varchar(50) DEFAULT NULL,
  `img` text DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.barang_toko: 9 rows
/*!40000 ALTER TABLE `barang_toko` DISABLE KEYS */;
INSERT IGNORE INTO `barang_toko` (`id`, `id_toko`, `nama_barang`, `keterangan`, `harga`, `img`, `created_date`, `updated_date`, `is_deleted`) VALUES
	(1, 4, 'Mouse Wireless', 'LOGITECH WIRELESS MOUSE M170 ASLI MOUSE WIRELESS ORIGINAL BERGARANSI - Hitam', '125000', 'http://nama_domain_anda.com/apartemen/assets/img/toko/b60f42c7121badb0c5ee212304b5fadd.jpg', '2020-05-22 10:37:22', '2020-05-23 10:34:18', 0),
	(2, 4, 'charger android', '', '85000', 'http://nama_domain_anda.com/apartemen/assets/img/toko/uploaded_4199305e7d5ae4e19a8365d12dd01144.jpg', '2020-05-22 10:37:47', NULL, 0),
	(3, 4, 'earphone', '', '70000', 'http://nama_domain_anda.com/apartemen/assets/img/toko/9382ea050fd768e83cb860b1a20b8bdd.jpg', '2020-05-22 10:38:06', NULL, 0),
	(4, 4, 'Gantungan Kunci', '', '5000', 'http://nama_domain_anda.com/apartemen/assets/img/toko/8db8bcca50dab1779179.jpg', '2020-05-23 10:28:17', '2020-06-07 16:15:47', 0),
	(5, 5, 'Galon', '', '7000', 'http://nama_domain_anda.com/apartemen/assets/img/toko/11316575_0a3f995e-0c36-4557-ac10-d1f9a8436ddd_400_400.jpg', '2020-05-23 10:28:17', '2020-05-23 10:41:04', 0),
	(6, 4, 'Mouse', '', '110000', 'http://nama_domain_anda.com/apartemen/assets/img/toko/558881ca3c5d0c446703.jpg', '2020-05-29 08:40:20', '2020-06-07 09:54:38', 0),
	(7, 4, 'Batere', '', '15000', 'http://nama_domain_anda.com/apartemen/assets/img/toko/3765e2f6da7a2c557e27.jpg', '2020-05-30 17:09:42', '2020-06-06 16:51:47', 1),
	(11, 4, 'hh', 'yyyiiuuuu', '123', 'http://nama_domain_anda.com/apartemen/assets/img/toko/7ebdf64e662fa2cb347c.jpg', '2020-06-07 11:21:01', '2020-06-07 16:06:07', 1),
	(12, 4, 'bbbuuu', '', '222', 'http://nama_domain_anda.com/apartemen/assets/img/toko/eebfa79a3edaf7266a1c.jpg', '2020-06-07 11:26:08', '2020-06-07 11:30:13', 1);
/*!40000 ALTER TABLE `barang_toko` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.belanja_toko
CREATE TABLE IF NOT EXISTS `belanja_toko` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `jumlah_barang` int(11) DEFAULT NULL,
  `total_harga` varchar(50) DEFAULT NULL,
  `subtotal` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT '0' COMMENT '0=belum lunas, 1=checkout, 2=lunas. 3=diantar, 4=diterima',
  `kode_transaksi` varchar(50) DEFAULT '0',
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.belanja_toko: 8 rows
/*!40000 ALTER TABLE `belanja_toko` DISABLE KEYS */;
INSERT IGNORE INTO `belanja_toko` (`id`, `id_user`, `id_barang`, `jumlah_barang`, `total_harga`, `subtotal`, `status`, `kode_transaksi`, `created_date`) VALUES
	(33, 1, 5, 1, '7000', 7000, '0', '0', '2020-06-17 16:03:49'),
	(32, 1, 6, 1, '110000', 110000, '1', '1-20200617151905TK', '2020-06-17 15:14:56'),
	(31, 1, 6, 1, '110000', 110000, '1', '1-20200616075954TK', '2020-06-15 21:42:59'),
	(19, 1, 6, 1, '110000', 110000, '1', '1-20200617073005TK', '2020-06-14 21:42:50'),
	(23, 1, 6, 1, '110000', 192000, '1', '1-20200615203912TK', '2020-06-15 07:47:24'),
	(26, 1, 5, 1, '7000', 192000, '1', '1-20200615203912TK', '2020-06-15 13:57:33'),
	(27, 1, 4, 1, '5000', 192000, '1', '1-20200615203912TK', '2020-06-15 13:59:34'),
	(30, 1, 3, 1, '70000', 192000, '1', '1-20200615203912TK', '2020-06-15 20:38:26');
/*!40000 ALTER TABLE `belanja_toko` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.emergency
CREATE TABLE IF NOT EXISTS `emergency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `insert_date` datetime NOT NULL,
  `is_closed` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.emergency: 10 rows
/*!40000 ALTER TABLE `emergency` DISABLE KEYS */;
INSERT IGNORE INTO `emergency` (`id`, `user_id`, `insert_date`, `is_closed`) VALUES
	(15, 1, '2020-06-17 16:00:57', 0),
	(14, 1, '2020-06-09 14:15:10', 0),
	(13, 1, '2020-06-09 13:25:26', 0),
	(12, 1, '2020-06-09 13:23:11', 0),
	(11, 1, '2020-06-09 09:53:27', 0),
	(10, 1, '2020-06-03 15:51:19', 0),
	(7, 1, '2020-05-20 10:51:57', 1),
	(8, 1, '2020-05-21 04:14:20', 1),
	(9, 1, '2020-05-21 07:00:57', 1),
	(16, 1, '2020-06-17 16:37:23', 0);
/*!40000 ALTER TABLE `emergency` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.fasilitas_gedung
CREATE TABLE IF NOT EXISTS `fasilitas_gedung` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fasilitas` varchar(80) DEFAULT NULL,
  `biaya_sewa` varchar(80) DEFAULT NULL,
  `img` text DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.fasilitas_gedung: 3 rows
/*!40000 ALTER TABLE `fasilitas_gedung` DISABLE KEYS */;
INSERT IGNORE INTO `fasilitas_gedung` (`id`, `fasilitas`, `biaya_sewa`, `img`, `is_deleted`) VALUES
	(1, 'Barberque ', '100000', 'http://nama_domain_anda.com/apartemen/assets/img/fasilitas/33206834804.jpg', 0),
	(2, 'kolam renang', '200000', 'http://nama_domain_anda.com/apartemen/assets/img/fasilitas/swimming-pool.jpg', 0),
	(3, 'Gym ', '150000', 'http://nama_domain_anda.com/apartemen/assets/img/fasilitas/gym.jpg', 0);
/*!40000 ALTER TABLE `fasilitas_gedung` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.gedung
CREATE TABLE IF NOT EXISTS `gedung` (
  `id_gedung` int(11) NOT NULL AUTO_INCREMENT,
  `kode_gedung` varchar(10) DEFAULT NULL,
  `nama_gedung` varchar(100) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `kota` varchar(100) DEFAULT NULL,
  `id_apt` int(11) DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  PRIMARY KEY (`id_gedung`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.gedung: 4 rows
/*!40000 ALTER TABLE `gedung` DISABLE KEYS */;
INSERT IGNORE INTO `gedung` (`id_gedung`, `kode_gedung`, `nama_gedung`, `alamat`, `kota`, `id_apt`, `is_deleted`) VALUES
	(3, '001', 'Tower 1', 'Jl. Senopati No. 8, Senayan', 'Jakarta', 1, 0),
	(4, '002', 'Tower 2', 'Jl. Senopati No. 8, Senayan', 'Jakarta', 1, 0),
	(5, '003', 'Alamanda ', 'Jl. Pesanggrahan ', 'Jakarta Barat', 3, 0),
	(6, '004', 'Marigold', 'Jl. Pesanggrahan ', 'Jakarta Barat', 3, 0);
/*!40000 ALTER TABLE `gedung` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.informasi_generic
CREATE TABLE IF NOT EXISTS `informasi_generic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `description` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.informasi_generic: 5 rows
/*!40000 ALTER TABLE `informasi_generic` DISABLE KEYS */;
INSERT IGNORE INTO `informasi_generic` (`id`, `title`, `description`) VALUES
	(1, 'Wifi Password', '123'),
	(2, 'Jam buka GYM', '08.00 WIB'),
	(5, 'Toko Buka dan tutup', '<p>Buka jam 07.00, tutup jam 22.00</p>'),
	(6, 'Nomor Rekening', '234 2145 1231'),
	(7, 'Nomor Rekening', '234 2145 1231');
/*!40000 ALTER TABLE `informasi_generic` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.invoice
CREATE TABLE IF NOT EXISTS `invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_invoice` varchar(50) DEFAULT NULL,
  `invoice_date` datetime DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `total` varchar(50) DEFAULT NULL,
  `is_paid` int(11) DEFAULT 0,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.invoice: 3 rows
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
INSERT IGNORE INTO `invoice` (`id`, `no_invoice`, `invoice_date`, `id_user`, `total`, `is_paid`, `created_date`) VALUES
	(1, '123456', '2020-05-20 14:42:00', 1, '1500000', 1, '2020-05-20 14:42:04'),
	(2, '123457', '2020-04-20 14:42:00', 1, '1500000', 1, '2020-04-20 14:42:04'),
	(3, '123458', '2020-03-20 14:42:00', 1, '1500000', 1, '2020-03-20 14:42:04');
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.jadwal_kerja
CREATE TABLE IF NOT EXISTS `jadwal_kerja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` varchar(50) DEFAULT NULL,
  `shift` int(11) DEFAULT NULL,
  `start_date` varchar(100) DEFAULT NULL,
  `end_date` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.jadwal_kerja: 8 rows
/*!40000 ALTER TABLE `jadwal_kerja` DISABLE KEYS */;
INSERT IGNORE INTO `jadwal_kerja` (`id`, `id_user`, `shift`, `start_date`, `end_date`) VALUES
	(2, '8', 2, '2020-06-07', '2020-06-14'),
	(3, '9', 2, '2020-06-07', '2020-06-14'),
	(4, '10', 1, '2020-06-07', '2020-06-14'),
	(5, '11', 1, '2020-06-07', '2020-06-14'),
	(6, '12', 2, '2020-06-07', '2020-06-14'),
	(7, '13', 2, '2020-06-07', '2020-06-14'),
	(8, '14', 1, '2020-06-07', '2020-06-14'),
	(10, '7', 1, '2020-06-14', '2020-06-20');
/*!40000 ALTER TABLE `jadwal_kerja` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.keluarga_user
CREATE TABLE IF NOT EXISTS `keluarga_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `nama` varchar(100) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `jk` varchar(50) DEFAULT NULL,
  `hubungan` varchar(50) DEFAULT NULL,
  `img` text DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table u6255547_apartemen.keluarga_user: 5 rows
/*!40000 ALTER TABLE `keluarga_user` DISABLE KEYS */;
INSERT IGNORE INTO `keluarga_user` (`id`, `user_id`, `nama`, `tgl_lahir`, `jk`, `hubungan`, `img`, `is_deleted`) VALUES
	(1, 1, 'Khairunnisa', '1993-09-09', 'Perempuan', 'anak', 'http://nama_domain_anda.com/apartemen/assets/img/user/cc076df012fa27a24645.png', 0),
	(2, 1, 'Ismail', '2016-07-25', 'Laki-laki', 'Anak', 'http://nama_domain_anda.com/apartemen/assets/img/user/887779c6d3711981dbc9.jpg', 0),
	(3, 1, 'Indah', '2019-01-25', 'Perempuan', 'Anak', 'http://nama_domain_anda.com/apartemen/assets/img/user/769a6fc959b4bfbc42a4.png', 0),
	(5, 3, 'Anissa', '1994-05-03', 'Perempuan', 'istri', 'http://nama_domain_anda.com/apartemen/assets/img/user/3300c4138272ff479006.png', 0),
	(7, 3, 'Putri', '2014-05-03', 'Perempuan', 'Anak', 'http://nama_domain_anda.com/apartemen/assets/img/user/d54be301282ffbd54e20.png', 0);
/*!40000 ALTER TABLE `keluarga_user` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.level_user
CREATE TABLE IF NOT EXISTS `level_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(50) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.level_user: 15 rows
/*!40000 ALTER TABLE `level_user` DISABLE KEYS */;
INSERT IGNORE INTO `level_user` (`id`, `level_name`) VALUES
	(1, 'Super Admin'),
	(2, 'Admin'),
	(3, 'General Manager'),
	(4, 'Finance Manager'),
	(5, 'Finance'),
	(6, 'Maintenance Manager'),
	(7, 'Maintenance SPV'),
	(8, 'Maintenance Team'),
	(9, 'Cleaning SPV'),
	(10, 'Cleaning Team'),
	(11, 'PPRS Head'),
	(12, 'PPRS'),
	(13, 'Support SPV'),
	(14, 'Support Team'),
	(15, 'Security');
/*!40000 ALTER TABLE `level_user` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.log_transaksi
CREATE TABLE IF NOT EXISTS `log_transaksi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` timestamp NULL DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `amount` varchar(50) DEFAULT NULL,
  `amount_int` varchar(50) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `balance` text DEFAULT NULL,
  `jenis_transaksi` text DEFAULT NULL,
  `res_transaction_check` text DEFAULT NULL,
  `msg_cek_mutasi` text DEFAULT NULL,
  `json` text DEFAULT NULL,
  `incomingApiSignature` text DEFAULT NULL,
  `post` text DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.log_transaksi: 1 rows
/*!40000 ALTER TABLE `log_transaksi` DISABLE KEYS */;
INSERT IGNORE INTO `log_transaksi` (`id`, `time`, `type`, `amount`, `amount_int`, `description`, `balance`, `jenis_transaksi`, `res_transaction_check`, `msg_cek_mutasi`, `json`, `incomingApiSignature`, `post`, `created_date`) VALUES
	(11, '0000-00-00 00:00:00', '', '', '', '', '', '', '', 'Invalid Signature', '', '', '', '2020-06-15 17:38:50');
/*!40000 ALTER TABLE `log_transaksi` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.makanan_restaurant
CREATE TABLE IF NOT EXISTS `makanan_restaurant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_resto` int(11) NOT NULL DEFAULT 0,
  `nama_makanan` varchar(50) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `harga` varchar(50) DEFAULT NULL,
  `img` text DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.makanan_restaurant: 5 rows
/*!40000 ALTER TABLE `makanan_restaurant` DISABLE KEYS */;
INSERT IGNORE INTO `makanan_restaurant` (`id`, `id_resto`, `nama_makanan`, `keterangan`, `harga`, `img`, `created_date`, `updated_date`, `is_deleted`) VALUES
	(1, 5, 'Mie Aceh', 'a', '25000', 'http://nama_domain_anda.com/apartemen/assets/img/resto/4411662=s1280=h960.jpg', '2020-05-22 10:38:29', NULL, 0),
	(2, 5, 'Kebab', 's', '18000', 'http://nama_domain_anda.com/apartemen/assets/img/resto/d2ba5c181ae79199e811.jpg', '2020-05-22 10:38:42', '2020-05-30 22:35:37', 0),
	(3, 5, 'Ayam Geprek', 'x', '20000', 'http://nama_domain_anda.com/apartemen/assets/img/resto/Resep-Ayam-Geprek-Crispy.jpg', '2020-05-22 10:38:55', '2020-05-23 10:40:16', 0),
	(4, 4, 'Rendang', 'c', '8000', 'http://nama_domain_anda.com/apartemen/assets/img/resto/57c9eead51200371f17f.jpg', '2020-05-23 10:25:32', '2020-05-30 22:37:49', 0),
	(5, 4, 'wwwwrrrr', 'z', '100001', 'http://nama_domain_anda.com/apartemen/assets/img/resto/38c6b3d5bc0fe309b235.jpg', '2020-06-07 10:25:28', '2020-06-07 10:25:48', 1);
/*!40000 ALTER TABLE `makanan_restaurant` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.messages
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sentby` varchar(50) DEFAULT NULL,
  `sentto` varchar(50) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `parent_id` varchar(50) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `is_read` enum('1','0') DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.messages: 5 rows
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT IGNORE INTO `messages` (`id`, `sentby`, `sentto`, `message`, `parent_id`, `datetime`, `is_read`) VALUES
	(1, 'admin', '2', 'Assalamu\'alaikum', NULL, '2020-05-25 18:29:39', '1'),
	(2, '2', 'admin', 'Wa\'alaikumsalam warahmatullah', '1', '2020-05-25 18:29:39', '1'),
	(3, 'admin', '1', 'Pagi', NULL, '2020-05-29 09:11:03', '0'),
	(4, '1', 'admin', 'Pagi Juga', '3', '2020-05-29 09:11:03', '1'),
	(6, '2', 'admin', 'Apa kabar', '1', '2020-05-26 18:29:39', '1');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.m_jadwal
CREATE TABLE IF NOT EXISTS `m_jadwal` (
  `IDJADWAL` int(11) NOT NULL AUTO_INCREMENT,
  `IDGEDUNG` int(11) DEFAULT NULL,
  `IDSTAF` int(10) DEFAULT NULL,
  `TGL` date DEFAULT NULL,
  `SHIFT` enum('Pagi','Siang','Malam') DEFAULT 'Pagi',
  `HAPUS` int(11) DEFAULT 0,
  PRIMARY KEY (`IDJADWAL`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.m_jadwal: 0 rows
/*!40000 ALTER TABLE `m_jadwal` DISABLE KEYS */;
/*!40000 ALTER TABLE `m_jadwal` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.m_penghuni
CREATE TABLE IF NOT EXISTS `m_penghuni` (
  `IDPENGHUNI` int(11) NOT NULL AUTO_INCREMENT,
  `NAMA` varchar(100) DEFAULT NULL,
  `KODE` varchar(50) DEFAULT NULL,
  `NIK` varchar(50) DEFAULT NULL,
  `JK` enum('Laki-laki','Perempuan') DEFAULT 'Laki-laki',
  `TEMPATLAHIR` varchar(100) DEFAULT NULL,
  `TANGALLAHIR` date DEFAULT NULL,
  `STATUS_KEL` varchar(255) DEFAULT NULL,
  `HP` varchar(20) DEFAULT NULL,
  `EMAIL` varchar(100) DEFAULT NULL,
  `STATUS_PENGHUNI` int(11) DEFAULT 1 COMMENT '1 = Aktif, 2 = Non Aktif',
  `IDGEDUNG` int(11) DEFAULT NULL,
  `IDUNIT` int(11) DEFAULT NULL,
  `HAPUS` int(11) DEFAULT 0,
  PRIMARY KEY (`IDPENGHUNI`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.m_penghuni: 1 rows
/*!40000 ALTER TABLE `m_penghuni` DISABLE KEYS */;
INSERT IGNORE INTO `m_penghuni` (`IDPENGHUNI`, `NAMA`, `KODE`, `NIK`, `JK`, `TEMPATLAHIR`, `TANGALLAHIR`, `STATUS_KEL`, `HP`, `EMAIL`, `STATUS_PENGHUNI`, `IDGEDUNG`, `IDUNIT`, `HAPUS`) VALUES
	(1, 'Tes Penghuni', '001', '321122233030000', 'Laki-laki', 'Depok', '2020-04-09', 'Bpk', '082113324411', 'gunawan@gmail.com', 1, 3, 1, 0);
/*!40000 ALTER TABLE `m_penghuni` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.m_staff
CREATE TABLE IF NOT EXISTS `m_staff` (
  `IDSTAF` int(11) NOT NULL AUTO_INCREMENT,
  `NAMA` varchar(255) DEFAULT NULL,
  `ALAMAT` varchar(255) DEFAULT NULL,
  `KOTA` varchar(100) DEFAULT NULL,
  `HP` varchar(50) DEFAULT NULL,
  `KATEGORI` enum('Kebersihan','Keamanan','Maintenance') DEFAULT 'Kebersihan',
  `IDGEDUNG` int(11) DEFAULT NULL,
  `STATUS` int(11) DEFAULT 0,
  `EMAIL` varchar(100) DEFAULT NULL,
  `PASSWORD` varchar(100) DEFAULT NULL,
  `HAPUS` int(11) DEFAULT 0,
  PRIMARY KEY (`IDSTAF`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.m_staff: 2 rows
/*!40000 ALTER TABLE `m_staff` DISABLE KEYS */;
INSERT IGNORE INTO `m_staff` (`IDSTAF`, `NAMA`, `ALAMAT`, `KOTA`, `HP`, `KATEGORI`, `IDGEDUNG`, `STATUS`, `EMAIL`, `PASSWORD`, `HAPUS`) VALUES
	(1, 'Sumanto', 'Jl.Anjasmoro 43 Desa Turirejo Kec.Lawang Malang', 'Jakarta', '81333387700', 'Kebersihan', 3, 1, 'mail.sonicmaster@gmail.com', '123456', 0),
	(2, 'gunawan', 'pabuaran bojonggede', 'Depok', '082113324411', 'Maintenance', 3, 1, 'gunawan@gmail.com', '123456', 0);
/*!40000 ALTER TABLE `m_staff` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.m_tenant
CREATE TABLE IF NOT EXISTS `m_tenant` (
  `IDTENANT` int(11) NOT NULL AUTO_INCREMENT,
  `NAMA` varchar(100) DEFAULT NULL,
  `DISKIRPSI` varchar(255) DEFAULT NULL,
  `KATEGORI` varchar(255) DEFAULT NULL,
  `HP` varchar(20) DEFAULT NULL,
  `EMAIL` varchar(100) DEFAULT NULL,
  `PASSWORD` varchar(100) DEFAULT NULL,
  `STATUS` int(11) DEFAULT 1,
  `IDGEDUNG` int(11) DEFAULT NULL,
  `HAPUS` int(11) DEFAULT 0,
  PRIMARY KEY (`IDTENANT`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.m_tenant: 0 rows
/*!40000 ALTER TABLE `m_tenant` DISABLE KEYS */;
/*!40000 ALTER TABLE `m_tenant` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.m_transaksi
CREATE TABLE IF NOT EXISTS `m_transaksi` (
  `IDTRX` int(11) NOT NULL AUTO_INCREMENT,
  `BL` varchar(20) DEFAULT NULL,
  `TH` varchar(10) DEFAULT NULL,
  `METER` varchar(50) DEFAULT NULL,
  `NOMINAL` int(11) DEFAULT NULL,
  `LINAS` int(11) DEFAULT 0,
  `IDGEDUNG` int(11) DEFAULT NULL,
  `IDUNIT` int(11) DEFAULT NULL,
  PRIMARY KEY (`IDTRX`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.m_transaksi: 1 rows
/*!40000 ALTER TABLE `m_transaksi` DISABLE KEYS */;
INSERT IGNORE INTO `m_transaksi` (`IDTRX`, `BL`, `TH`, `METER`, `NOMINAL`, `LINAS`, `IDGEDUNG`, `IDUNIT`) VALUES
	(1, '01', '2020', 'tes', 800000, 0, 3, 3);
/*!40000 ALTER TABLE `m_transaksi` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.notifikasi
CREATE TABLE IF NOT EXISTS `notifikasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL DEFAULT 0,
  `title` varchar(50) DEFAULT NULL,
  `desc` text DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `sent` int(11) DEFAULT 0,
  `is_deleted` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.notifikasi: 2 rows
/*!40000 ALTER TABLE `notifikasi` DISABLE KEYS */;
INSERT IGNORE INTO `notifikasi` (`id`, `id_user`, `title`, `desc`, `created_date`, `sent`, `is_deleted`) VALUES
	(1, 1, 'Perbaikan Lift', 'Pada tanggal 28 Mei 2020, Jam 10.00 - Selesai', '2020-05-23 10:45:06', 0, 0),
	(2, 2, 'Maintenance', '<p>Maintenance Lift</p>', NULL, 1, 0);
/*!40000 ALTER TABLE `notifikasi` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.order_makanan
CREATE TABLE IF NOT EXISTS `order_makanan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_makanan` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `total_harga` varchar(50) DEFAULT NULL,
  `subtotal` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT '0' COMMENT '0=belum lunas, 1=checkout, 2=lunas. 3=diantar, 4=diterima',
  `kode_transaksi` varchar(50) DEFAULT '0',
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.order_makanan: 1 rows
/*!40000 ALTER TABLE `order_makanan` DISABLE KEYS */;
INSERT IGNORE INTO `order_makanan` (`id`, `id_user`, `id_makanan`, `jumlah`, `total_harga`, `subtotal`, `status`, `kode_transaksi`, `created_date`) VALUES
	(9, 1, 2, 2, '36000', '36000', '1', '1-20200617160154TK', '2020-06-13 20:25:31');
/*!40000 ALTER TABLE `order_makanan` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.pengumuman
CREATE TABLE IF NOT EXISTS `pengumuman` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `desc` text DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.pengumuman: 1 rows
/*!40000 ALTER TABLE `pengumuman` DISABLE KEYS */;
INSERT IGNORE INTO `pengumuman` (`id`, `title`, `desc`, `created_date`) VALUES
	(1, 'service lift', 'Tanggal 22 Mei 2020 Jam 08.00 - 12.00 WIB', '2020-05-21 11:11:56');
/*!40000 ALTER TABLE `pengumuman` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.pic
CREATE TABLE IF NOT EXISTS `pic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pic` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.pic: 1 rows
/*!40000 ALTER TABLE `pic` DISABLE KEYS */;
INSERT IGNORE INTO `pic` (`id`, `nama_pic`) VALUES
	(1, 'Feri');
/*!40000 ALTER TABLE `pic` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.request_fasilitas
CREATE TABLE IF NOT EXISTS `request_fasilitas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fasilitas_id` int(11) DEFAULT NULL,
  `request_start` datetime DEFAULT NULL,
  `request_end` datetime DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `status` varchar(12) DEFAULT 'Submitted',
  `biaya` varchar(12) DEFAULT NULL,
  `is_paid` int(11) DEFAULT 0,
  `invoice_no` varchar(50) DEFAULT NULL,
  `tanggal_bayar` datetime DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.request_fasilitas: 8 rows
/*!40000 ALTER TABLE `request_fasilitas` DISABLE KEYS */;
INSERT IGNORE INTO `request_fasilitas` (`id`, `fasilitas_id`, `request_start`, `request_end`, `id_user`, `status`, `biaya`, `is_paid`, `invoice_no`, `tanggal_bayar`, `created_date`) VALUES
	(1, 1, '2020-02-07 09:48:00', '2020-02-07 19:48:00', 1, 'Finished', '100000', 1, '123456', '2020-05-20 14:57:00', '2020-05-19 14:44:08'),
	(3, 1, '2020-02-07 09:48:00', '2020-02-07 19:48:00', 1, 'Submitted', '100000', 0, NULL, NULL, '2020-05-20 23:21:58'),
	(2, 1, '2020-05-20 21:51:00', '2020-05-20 22:51:00', 1, 'Submitted', '100000', 0, NULL, NULL, '2020-05-20 14:52:01'),
	(4, 2, '2020-05-21 12:50:00', '2020-05-21 12:54:00', 1, 'Accept', '100000', 0, NULL, NULL, '2020-05-21 05:50:33'),
	(5, 3, '2020-06-03 16:17:00', '2020-06-06 16:17:00', 1, 'Submitted', NULL, 0, NULL, NULL, '2020-06-03 16:17:23'),
	(6, 2, '2020-06-09 02:05:00', '2020-06-09 02:05:00', 1, 'Submitted', NULL, 0, NULL, NULL, '2020-06-09 09:05:59'),
	(7, 2, '2020-06-09 02:05:00', '2020-06-09 02:06:00', 1, 'Submitted', NULL, 0, NULL, NULL, '2020-06-09 09:06:11'),
	(8, 3, '2020-06-15 08:42:00', '2020-06-16 08:42:00', 1, 'Submitted', NULL, 0, NULL, NULL, '2020-06-15 08:42:55');
/*!40000 ALTER TABLE `request_fasilitas` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.request_maintenance
CREATE TABLE IF NOT EXISTS `request_maintenance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL DEFAULT 0,
  `request` text DEFAULT NULL,
  `id_teknisi` int(5) DEFAULT NULL,
  `charge` varchar(50) DEFAULT NULL,
  `is_paid` int(11) DEFAULT 0,
  `status` varchar(50) DEFAULT 'Submitted',
  `invoice_no` varchar(50) DEFAULT NULL,
  `tanggal_bayar` datetime DEFAULT NULL,
  `request_date` datetime DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.request_maintenance: 15 rows
/*!40000 ALTER TABLE `request_maintenance` DISABLE KEYS */;
INSERT IGNORE INTO `request_maintenance` (`id`, `id_user`, `request`, `id_teknisi`, `charge`, `is_paid`, `status`, `invoice_no`, `tanggal_bayar`, `request_date`, `created_date`) VALUES
	(1, 1, 'Maintenance AC', 1, '50000', 1, 'selesai', '123456', '2020-05-20 14:57:07', '2020-02-07 09:48:00', '2020-05-20 02:25:00'),
	(2, 1, 'Maintenance AC', NULL, '50000', 0, 'Submitted', NULL, NULL, '2020-02-07 20:14:00', '2020-05-20 13:14:46'),
	(3, 1, 'Maintenance AC', NULL, '50000', 0, 'Submitted', NULL, NULL, '2020-02-07 20:14:00', '2020-05-20 13:31:59'),
	(4, 1, 'aaaa', 3, '50000', 0, 'Submitted', NULL, NULL, '2020-05-20 20:33:00', '2020-05-20 13:34:04'),
	(5, 1, 'test', NULL, '50000', 0, 'Submitted', NULL, NULL, '2020-05-21 10:24:00', '2020-05-21 03:24:23'),
	(6, 1, 'test 2', 1, '50000', 0, 'Submitted', NULL, NULL, '2020-05-21 10:24:00', '2020-05-21 03:25:02'),
	(7, 1, 'aaaa', NULL, '50000', 0, 'Submitted', NULL, NULL, '2020-05-21 10:28:00', '2020-05-21 03:28:33'),
	(8, 1, 'aaaa', NULL, '50000', 0, 'Submitted', NULL, NULL, '2020-05-21 10:28:00', '2020-05-21 03:28:46'),
	(9, 1, 'aaaa', 3, '50000', 0, 'Submitted', NULL, NULL, '2020-05-21 10:28:00', '2020-05-21 03:28:49'),
	(10, 1, 'ffgb', 4, '50000', 0, 'Submitted', NULL, NULL, '2020-05-21 10:30:00', '2020-05-21 03:30:47'),
	(11, 1, 'I have fxxyxtxt', 1, '50000', 0, 'Submitted', NULL, NULL, '2020-05-21 10:33:00', '2020-05-21 03:33:51'),
	(12, 1, 'test eee', 1, '50000', 0, 'Accepted', NULL, NULL, '2020-05-21 12:09:00', '2020-05-21 05:09:30'),
	(13, 1, 'saklar lampu ruang tamu mati', 3, NULL, 0, 'Accepted', NULL, NULL, '2020-06-04 16:06:00', '2020-06-03 16:06:35'),
	(14, 1, 'ac', NULL, NULL, 0, 'Submitted', NULL, NULL, '2020-06-15 16:24:00', '2020-06-15 16:16:03'),
	(15, 1, 'atap bocor', NULL, NULL, 0, 'Submitted', NULL, NULL, '2020-06-15 16:24:00', '2020-06-15 16:16:23');
/*!40000 ALTER TABLE `request_maintenance` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.request_room_service
CREATE TABLE IF NOT EXISTS `request_room_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL DEFAULT 0,
  `request` text DEFAULT NULL,
  `charge` varchar(50) DEFAULT NULL,
  `is_paid` int(11) DEFAULT 0,
  `status` varchar(50) DEFAULT 'Submitted',
  `invoice_no` varchar(50) DEFAULT NULL,
  `tanggal_bayar` datetime DEFAULT NULL,
  `request_date` datetime DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table u6255547_apartemen.request_room_service: 6 rows
/*!40000 ALTER TABLE `request_room_service` DISABLE KEYS */;
INSERT IGNORE INTO `request_room_service` (`id`, `id_user`, `request`, `charge`, `is_paid`, `status`, `invoice_no`, `tanggal_bayar`, `request_date`, `created_date`) VALUES
	(1, 1, 'Maintenance AC', '50000', 1, 'Finished', '123456', '2020-05-20 14:57:25', '2020-02-07 09:48:00', '2020-05-20 02:25:00'),
	(2, 1, 'Maintenance AC', '50000', 1, 'Finished', '123456', '2020-04-20 14:57:25', '2020-02-07 09:48:00', '2020-05-20 02:25:00'),
	(3, 1, 'laundry', '50000', 0, 'Finished', NULL, NULL, '2020-04-21 10:17:25', '2020-05-21 00:27:04'),
	(4, 1, 'laundry i', '50000', 0, 'Submitted', NULL, NULL, '2020-05-21 14:28:00', '2020-05-21 07:28:39'),
	(5, 1, 'bersihkan kamar dong ', NULL, 0, 'Submitted', NULL, NULL, '2020-06-03 16:21:00', '2020-06-03 16:21:08'),
	(6, 1, 'laundry', NULL, 0, 'Submitted', NULL, NULL, '2020-06-09 19:36:00', '2020-06-09 19:36:16');
/*!40000 ALTER TABLE `request_room_service` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.restaurant
CREATE TABLE IF NOT EXISTS `restaurant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL DEFAULT 0,
  `nama_resto` varchar(50) NOT NULL DEFAULT '0',
  `img` text NOT NULL DEFAULT '0',
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `is_approve` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table u6255547_apartemen.restaurant: 3 rows
/*!40000 ALTER TABLE `restaurant` DISABLE KEYS */;
INSERT IGNORE INTO `restaurant` (`id`, `id_user`, `nama_resto`, `img`, `created_date`, `updated_date`, `is_deleted`, `is_approve`) VALUES
	(5, 2, 'Sederhana', 'http://nama_domain_anda.com/apartemen/assets/img/resto/icon/e2c0082a7f8d18c10d3c.jpg', '2020-05-23 09:57:37', '2020-05-23 17:13:15', 0, 1),
	(4, 1, 'Restu Bundo', 'http://nama_domain_anda.com/apartemen/assets/img/resto/icon/Mediacorp_2015.png', '2020-05-23 02:56:46', '2020-05-23 10:08:07', 0, 1),
	(6, 3, 'Bahagia', 'http://nama_domain_anda.com/apartemen/assets/img/resto/icon/1119f85eaf563a028853.jpg', '2020-05-23 12:38:39', '2020-05-30 17:23:41', 0, 1);
/*!40000 ALTER TABLE `restaurant` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.role
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table u6255547_apartemen.role: 5 rows
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT IGNORE INTO `role` (`id`, `role_name`) VALUES
	(1, 'Developer'),
	(2, 'Administrator'),
	(3, 'Customer Support'),
	(4, 'Human Resource'),
	(5, 'Finance');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.support_staff
CREATE TABLE IF NOT EXISTS `support_staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_staff` varchar(50) DEFAULT NULL,
  `email` text DEFAULT NULL,
  `role` varchar(50) DEFAULT NULL,
  `img` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table u6255547_apartemen.support_staff: 4 rows
/*!40000 ALTER TABLE `support_staff` DISABLE KEYS */;
INSERT IGNORE INTO `support_staff` (`id`, `nama_staff`, `email`, `role`, `img`) VALUES
	(1, 'Fachruddin', 'fachruddin@gmail.com', '2', 'http://nama_domain_anda.com/apartemen/assets/img/teknisi/users-vector-icon-png_260862.jpg'),
	(2, 'Umar', 'umar@gmail.com', '3', 'http://nama_domain_anda.com/apartemen/assets/img/teknisi/f5c32491db959a6ec93b.jpg'),
	(3, 'Utsman', 'utsman@gmail.com', '4', 'http://nama_domain_anda.com/apartemen/assets/img/teknisi/f0cbbd8f9d02328e9a43.jpg'),
	(4, 'Ferdinand', 'ferdi@moratelindo.co.id', '1', 'http://nama_domain_anda.com/apartemen/assets/img/support_staff/887779c6d3711981dbc9.jpg');
/*!40000 ALTER TABLE `support_staff` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.teknisi
CREATE TABLE IF NOT EXISTS `teknisi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_teknisi` varchar(50) DEFAULT NULL,
  `img` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.teknisi: 3 rows
/*!40000 ALTER TABLE `teknisi` DISABLE KEYS */;
INSERT IGNORE INTO `teknisi` (`id`, `nama_teknisi`, `img`) VALUES
	(1, 'Fachruddin', 'http://nama_domain_anda.com/apartemen/assets/img/teknisi/users-vector-icon-png_260862.jpg'),
	(3, 'Umar', 'http://nama_domain_anda.com/apartemen/assets/img/teknisi/f5c32491db959a6ec93b.jpg'),
	(4, 'Utsman', 'http://nama_domain_anda.com/apartemen/assets/img/teknisi/f0cbbd8f9d02328e9a43.jpg');
/*!40000 ALTER TABLE `teknisi` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.ticket
CREATE TABLE IF NOT EXISTS `ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `kode_tiket` varchar(50) NOT NULL DEFAULT '',
  `keterangan` text NOT NULL DEFAULT '',
  `level` varchar(50) NOT NULL DEFAULT '',
  `status` varchar(50) NOT NULL DEFAULT 'New',
  `pic_id` varchar(50) NOT NULL DEFAULT '',
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.ticket: 4 rows
/*!40000 ALTER TABLE `ticket` DISABLE KEYS */;
INSERT IGNORE INTO `ticket` (`id`, `id_user`, `kode_tiket`, `keterangan`, `level`, `status`, `pic_id`, `created_date`, `updated_date`) VALUES
	(1, 1, '00001/ticket/05/2020', 'AC Rusak', 'urgent', 'Selesai', '1', '2020-05-20 02:48:47', '0000-00-00 00:00:00'),
	(2, 1, '00002/ticket/05/2020', 'lampu penerangan jalan mati', 'urgent', 'Processed', '2', '2020-06-20 15:03:44', '0000-00-00 00:00:00'),
	(3, 1, '00003/ticket/05/2020', 'Engsel pintu copot', 'urgent', 'Processed', '2', '2020-06-06 15:03:44', '0000-00-00 00:00:00'),
	(4, 2, '00004/ticket/06/2020', 'AC Rusak', 'urgent', 'Processed', '3', '2020-06-01 13:48:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `ticket` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.ticket_file
CREATE TABLE IF NOT EXISTS `ticket_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_id` int(11) DEFAULT NULL,
  `img` text DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.ticket_file: 2 rows
/*!40000 ALTER TABLE `ticket_file` DISABLE KEYS */;
INSERT IGNORE INTO `ticket_file` (`id`, `ticket_id`, `img`, `created_date`) VALUES
	(2, 1, 'http://nama_domain_anda.com/apartemen/assets/img/f58debb6846fd64ef62f.jpg', '2020-05-21 06:45:31'),
	(3, 1, 'http://nama_domain_anda.com/apartemen/assets/img/9392094ee96fc4f57020.jpg', '2020-05-21 06:50:37');
/*!40000 ALTER TABLE `ticket_file` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.ticket_message
CREATE TABLE IF NOT EXISTS `ticket_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sent_by` varchar(50) NOT NULL DEFAULT '0',
  `ticket_id` int(11) NOT NULL,
  `message` varchar(50) NOT NULL DEFAULT '',
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table u6255547_apartemen.ticket_message: 7 rows
/*!40000 ALTER TABLE `ticket_message` DISABLE KEYS */;
INSERT IGNORE INTO `ticket_message` (`id`, `sent_by`, `ticket_id`, `message`, `created_date`, `updated_date`) VALUES
	(1, 'user', 4, 'Mohon bantuannya', '2020-05-20 02:48:47', '0000-00-00 00:00:00'),
	(2, 'user', 1, 'Mohon bantuannya', '2020-05-20 15:03:44', '0000-00-00 00:00:00'),
	(3, 'user', 2, 'Mohon bantuannya', '2020-05-20 15:03:44', '0000-00-00 00:00:00'),
	(4, 'user', 3, 'Mohon bantuannya', '2020-05-20 15:03:44', '0000-00-00 00:00:00'),
	(5, 'admin', 1, 'Mohon ditunggu, sedang kami lanjutkan ke staff :)', '2020-06-01 14:28:02', '0000-00-00 00:00:00'),
	(7, 'admin', 4, 'Mohon ditunggu, sedang kami lanjutkan ke staff', '2020-05-20 02:48:47', '0000-00-00 00:00:00'),
	(8, 'staff', 4, 'OK', '2020-05-20 02:48:47', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `ticket_message` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.toko
CREATE TABLE IF NOT EXISTS `toko` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL DEFAULT 0,
  `nama_toko` varchar(50) NOT NULL DEFAULT '0',
  `img` text NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `is_approve` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table u6255547_apartemen.toko: 5 rows
/*!40000 ALTER TABLE `toko` DISABLE KEYS */;
INSERT IGNORE INTO `toko` (`id`, `id_user`, `nama_toko`, `img`, `created_date`, `updated_date`, `is_deleted`, `is_approve`) VALUES
	(4, 1, 'Selalu Ada', 'http://nama_domain_anda.com/apartemen/assets/img/toko/icon/00a03f1886a75da854c3.png', '2020-05-23 09:58:51', '2020-05-23 17:11:25', 0, 1),
	(5, 2, 'Makmur', 'http://nama_domain_anda.com/apartemen/assets/img/toko/icon/a4d01afb3d512991300e.jpg', NULL, '2020-05-30 17:20:50', 0, 1),
	(6, 3, 'Jaya', 'http://nama_domain_anda.com/apartemen/assets/img/toko/icon/d9df2898ea8ae0c2edce.png', '2020-05-23 12:39:36', NULL, 0, 1),
	(7, 1, 'Jaya Makmur', 'http://nama_domain_anda.com/apartemen/assets/img/toko/icon/6ba8bc2869be919068f4.png', '2020-05-24 02:39:36', '2020-05-30 17:21:58', 0, 1),
	(8, 1, 'Berkah Shop', 'http://nama_domain_anda.com/apartemen/assets/img/toko/icon/9ef5c2478dd2631c19f5.png', '2020-05-24 08:02:40', '2020-05-30 17:22:42', 0, 1);
/*!40000 ALTER TABLE `toko` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.transaksi_belanja_toko
CREATE TABLE IF NOT EXISTS `transaksi_belanja_toko` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_transaksi` varchar(50) NOT NULL,
  `id_belanja` int(11) DEFAULT NULL,
  `grand_total` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT 'UNPAID',
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.transaksi_belanja_toko: 7 rows
/*!40000 ALTER TABLE `transaksi_belanja_toko` DISABLE KEYS */;
INSERT IGNORE INTO `transaksi_belanja_toko` (`id`, `kode_transaksi`, `id_belanja`, `grand_total`, `status`, `created_date`) VALUES
	(16, '1-20200615203912TK', NULL, '192992', 'UNPAID', '2020-06-15 20:39:12'),
	(15, '1-20200614214314TK', NULL, '110992', 'UNPAID', '2020-06-14 21:43:14'),
	(17, '1-20200616075954TK', NULL, '110992', 'UNPAID', '2020-06-16 07:59:54'),
	(10, '1', 1, '10100', 'UNPAID', '2020-06-14 18:18:58'),
	(18, '1-20200617072542TK', NULL, '110992', 'UNPAID', '2020-06-17 07:25:42'),
	(19, '1-20200617073005TK', NULL, '110992', 'UNPAID', '2020-06-17 07:30:05'),
	(20, '1-20200617151905TK', NULL, '110992', 'UNPAID', '2020-06-17 15:19:05');
/*!40000 ALTER TABLE `transaksi_belanja_toko` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.transaksi_order_makan
CREATE TABLE IF NOT EXISTS `transaksi_order_makan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_transaksi` varchar(50) NOT NULL,
  `id_order_makan` int(11) DEFAULT NULL,
  `grand_total` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT 'UNPAID',
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table u6255547_apartemen.transaksi_order_makan: 4 rows
/*!40000 ALTER TABLE `transaksi_order_makan` DISABLE KEYS */;
INSERT IGNORE INTO `transaksi_order_makan` (`id`, `kode_transaksi`, `id_order_makan`, `grand_total`, `status`, `created_date`) VALUES
	(1, '1-20200615064251TK', NULL, '34992', 'UNPAID', '2020-06-15 06:42:51'),
	(2, '1-20200616205131TK', NULL, '8992', 'UNPAID', '2020-06-16 20:51:31'),
	(3, '1-20200617074030TK', NULL, '18992', 'UNPAID', '2020-06-17 07:40:30'),
	(4, '1-20200617160154TK', NULL, '18992', 'UNPAID', '2020-06-17 16:01:54');
/*!40000 ALTER TABLE `transaksi_order_makan` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.unit
CREATE TABLE IF NOT EXISTS `unit` (
  `id_unit` int(11) NOT NULL AUTO_INCREMENT,
  `nama_unit` varchar(100) DEFAULT NULL,
  `nomor` varchar(100) DEFAULT NULL,
  `id_gedung` int(11) DEFAULT NULL,
  `spek` text DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `ket` varchar(255) DEFAULT NULL,
  `lantai` int(11) DEFAULT NULL,
  `biaya_sewa` int(11) DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `kode_unit` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_unit`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.unit: 4 rows
/*!40000 ALTER TABLE `unit` DISABLE KEYS */;
INSERT IGNORE INTO `unit` (`id_unit`, `nama_unit`, `nomor`, `id_gedung`, `spek`, `foto`, `ket`, `lantai`, `biaya_sewa`, `is_deleted`, `kode_unit`, `status`) VALUES
	(1, 'Spacious Studio', '201', 3, 'Hunian yang nyaman dengan ukuran studio di area Sunter, Jakarta Utara ini akan membuat Anda merasa seperti di rumah sendiri ', 'http://nama_domain_anda.com/apartemen/assets/img/unit/1dc33cbf695b4ddfcb11.jpg', 'Keterangan', 1, 3000000, 0, '', 1),
	(2, 'M', '101', 6, ' type 32', 'http://nama_domain_anda.com/apartemen/assets/img/unit/469483362ed926e8e8ba.jpg', ' ', 1, 4000000, 0, '001', 1),
	(3, 'Tes Unit', '3', 3, ' -', 'http://nama_domain_anda.com/apartemen/assets/img/unit/340a88da130cbfa039d0.jpg', ' -', 1, 5000000, 0, '002', 1),
	(4, 'Mars', '1', 4, ' -', 'http://nama_domain_anda.com/apartemen/assets/img/unit/b0b98b0a650a963337eb.jpg', ' -', 1, 5000000, 0, '002', 0);
/*!40000 ALTER TABLE `unit` ENABLE KEYS */;

-- Dumping structure for table u6255547_apartemen.user
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `phone_number` varchar(100) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `jk` varchar(50) DEFAULT NULL,
  `no_ktp` varchar(100) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `idunit` int(11) DEFAULT NULL,
  `img` text DEFAULT NULL,
  `nomor_rekening` text DEFAULT NULL,
  `nama_bank` text DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table u6255547_apartemen.user: 4 rows
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT IGNORE INTO `user` (`user_id`, `nama`, `username`, `email`, `password`, `phone_number`, `tgl_lahir`, `jk`, `no_ktp`, `level`, `status`, `idunit`, `img`, `nomor_rekening`, `nama_bank`, `is_deleted`) VALUES
	(1, 'Wahyu Artha', 'wahyuarta', 'wahyuartadigital@gmail.com', 'n3uhp66XeKSl', '08128378992', '1990-05-25', 'Laki-laki', '321122233030000', 1, 1, 1, 'http://nama_domain_anda.com/apartemen/assets/img/user/887779c6d3711981dbc9.jpg', '124432231345', 'BCA', 0),
	(2, 'Febu', 'febu', 'febu@gmail.com', 'n3uhp66XeKSl', '08118378221', '1990-07-25', 'Laki-laki', '321122233030001', 1, 1, 2, 'http://nama_domain_anda.com/apartemen/assets/img/user/887779c6d3711981dbc9.jpg', '124432231345', 'BCA', 0),
	(3, 'Acep', 'acep', 'acep@gmail.com', 'n3uhp66XeKSl', '08118378221', '1990-01-25', 'Laki-laki', '321122233030002', 1, 0, 3, 'http://nama_domain_anda.com/apartemen/assets/img/user/887779c6d3711981dbc9.jpg', '124432231345', 'BCA', 0),
	(4, 'ssss', 'febuharyanto', 'febuharyanto@gmail.com', NULL, '', NULL, 'Laki-laki', '1212121212', NULL, 4, NULL, 'http://nama_domain_anda.com/apartemen/assets/img/user/904b948017441468ce84.jpg', '124432231345', 'BCA', 0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping structure for view u6255547_apartemen.v_barang_toko
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_barang_toko` (
	`id` INT(11) NOT NULL,
	`nama_toko` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`nama_barang` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`id_toko` INT(11) NULL,
	`harga` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`img` TEXT NULL COLLATE 'latin1_swedish_ci',
	`is_deleted` INT(11) NULL,
	`keterangan` TEXT NOT NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- Dumping structure for view u6255547_apartemen.v_makanan
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_makanan` (
	`id` INT(11) NOT NULL,
	`nama_resto` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`keterangan` TEXT NULL COLLATE 'latin1_swedish_ci',
	`nama_makanan` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`id_resto` INT(11) NULL,
	`harga` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`img` TEXT NULL COLLATE 'latin1_swedish_ci',
	`is_deleted` INT(11) NULL
) ENGINE=MyISAM;

-- Dumping structure for view u6255547_apartemen.v_barang_toko
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_barang_toko`;
CREATE ALGORITHM=UNDEFINED DEFINER=`u6255547_apartemen`@`139.192.187.219` SQL SECURITY DEFINER VIEW `v_barang_toko` AS select `a`.`id` AS `id`,`b`.`nama_toko` AS `nama_toko`,`a`.`nama_barang` AS `nama_barang`,`b`.`id` AS `id_toko`,`a`.`harga` AS `harga`,`a`.`img` AS `img`,`a`.`is_deleted` AS `is_deleted`,`a`.`keterangan` AS `keterangan` from (`barang_toko` `a` left join `toko` `b` on(`a`.`id_toko` = `b`.`id`)) where `a`.`is_deleted` = '0' order by `a`.`id` desc;

-- Dumping structure for view u6255547_apartemen.v_makanan
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_makanan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`u6255547_apartemen`@`139.192.187.219` SQL SECURITY DEFINER VIEW `v_makanan` AS select `a`.`id` AS `id`,`b`.`nama_resto` AS `nama_resto`,`a`.`keterangan` AS `keterangan`,`a`.`nama_makanan` AS `nama_makanan`,`b`.`id` AS `id_resto`,`a`.`harga` AS `harga`,`a`.`img` AS `img`,`a`.`is_deleted` AS `is_deleted` from (`makanan_restaurant` `a` left join `restaurant` `b` on(`a`.`id_resto` = `b`.`id`)) where `a`.`is_deleted` = '0' order by `a`.`id` desc;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
