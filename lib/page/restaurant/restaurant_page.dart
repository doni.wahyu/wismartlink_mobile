import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/model/cartModel.dart';
import 'package:wismartlink/model/jumlahTransaksiToko.dart';
import 'package:wismartlink/page/home_page.dart';
import 'package:wismartlink/page/marketplace/barang_detail_page.dart';
import 'package:wismartlink/page/marketplace/barang_page.dart';
import 'package:wismartlink/page/marketplace/cart.dart';
import 'package:wismartlink/page/marketplace/cart_select.dart';
import 'package:wismartlink/page/marketplace/marketplace_register_page.dart';
import 'package:wismartlink/page/marketplace/pesananSaya.dart';
import 'package:wismartlink/page/restaurant/cartFood.dart';
import 'package:wismartlink/page/restaurant/makanan_detail_page.dart';

class RestaurantPage extends StatefulWidget {
  const RestaurantPage({Key key}) : super(key: key);

  @override
  _RestaurantPageController createState() => _RestaurantPageController();
}

class _RestaurantPageController extends State<RestaurantPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  bool isPunyaToko = false;
  List data;
  TextEditingController searchController = new TextEditingController();
  int counter = 0;
  int list = 0;
  String subttl;
  List<JumlahTransaksiToko> _requestList = [];

  @override
  void initState() {
    super.initState();
    _getRequestDetail();
    validatePunyaToko();
    getData();
    _getJmlCheckout();
  }

  _getJmlCheckout() async {
    var dio = new Dio();

    final url = API_URL;
    var param = {
      "id_user": await getSession("user_id"),
    };

    try {
      Response response;
      response = await dio.post(url + "jumlah_list_transaksi_resto_checkout",
          data: param,
          options: new Options(
            headers: {"Content-Type": "application/json"},
          ));
      // print('response.data[] ' + response.data['data']['jumlah'].toString());
      if (response.data['success'] == true) {
        setState(() {
          list = response.data['data']['list'];
          print("list " + list.toString());
        });
      } else {
        setState(() {
          list = 0;
        });
      }
    } on DioError catch (e) {
      print("CCC");
      if (e.response != null) {
        print(e.response.data);
        print(e.response.headers);
        print(e.response.request);
      } else {
        print(e.request);
        print(e.message);
      }
    }
  }

  void validatePunyaToko() async {
    String id_toko = await getSession("id_toko");
    if (id_toko != null) {
      if (id_toko != "") {
        setState(() {
          isPunyaToko = true;
        });
      }
    }
  }

  void getData() async {
    setState(() => isLoading = true);

    try {
      Response response = await new Dio().post(API_URL + 'list_makanan',
          data: new FormData.fromMap({"keyword": searchController.text}),
          options: Options(method: 'POST', responseType: ResponseType.json));

      setState(() => isLoading = false);

      if (response.toString() != '') {
        setState(() {
          data = response.data['data'];
        });
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    }
  }

  _getRequestDetail() async {
    var dio = new Dio();

    final url = API_URL;
    var param = {
      "id_user": await getSession("user_id"),
    };

    try {
      Response response;
      response = await dio.post(url + "jumlah_transaksi_resto_belum_lunas",
          data: param,
          options: new Options(
            headers: {"Content-Type": "application/json"},
          ));
      // print('response.data[] ' + response.data['data']['jumlah'].toString());
      if (response.data['success'] == true) {
        setState(() {
          counter = response.data['data']['jumlah'];
          subttl = response.data['data']['subtotal'];
        });
      } else {
        setState(() {
          counter = 0;
        });
      }
    } on DioError catch (e) {
      print("CCC");
      if (e.response != null) {
        print(e.response.data);
        print(e.response.headers);
        print(e.response.request);
      } else {
        print(e.request);
        print(e.message);
      }
    }
  }

  _listItem(CartModel cart) {
    return Column(
      children: <Widget>[
        Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
            ),
            child: Container(
              padding: EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(15.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      boxShadow: <BoxShadow>[
                        new BoxShadow(
                          color: Colors.black12,
                          blurRadius: 10.0,
                          offset: new Offset(0.0, 10.0),
                        ),
                      ],
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                          flex: 5,
                          child: Image.network(
                            cart.img,
                            width: 80.0,
                            height: 80.0,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        ),
                        Flexible(
                            flex: 5,
                            child: Align(
                              alignment: Alignment.topRight,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        margin:
                                            const EdgeInsets.only(left: 15.0),
                                        child: Text(
                                          cart.namaBarang,
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 14.0),
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        margin:
                                            const EdgeInsets.only(left: 15.0),
                                        child: Text(
                                          thousandSeparator(
                                              cart.totalHarga.toString()),
                                          style: TextStyle(
                                              color: Colors.green,
                                              fontSize: 13.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            )),
                      ],
                    ),
                  ),
                ],
              ),
            )),
      ],
    );
  }

  Widget _icon_cart() {
    if (counter == 0) {
      return Stack(
        children: <Widget>[
          new IconButton(
              icon: Icon(Icons.shopping_cart),
              onPressed: () {
                setState(() {
                  counter = 0;
                });
              }),
          counter != 0
              ? new Positioned(
                  right: 11,
                  top: 11,
                  child: new Container(
                    padding: EdgeInsets.all(2),
                    decoration: new BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(6),
                    ),
                    constraints: BoxConstraints(
                      minWidth: 14,
                      minHeight: 14,
                    ),
                    child: Text(
                      '$counter',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 8,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                )
              : new Container()
        ],
      );
    } else {
      return InkWell(
        onTap: () {
          Navigator.of(context).push(new MaterialPageRoute(builder: (_) {
            return new CartFood(counter.toString(), subttl.toString());
          }));
        },
        child: new Stack(
          children: <Widget>[
            new IconButton(
                icon: Icon(Icons.shopping_cart),
                onPressed: () {
                  setState(() {
                    counter = 0;
                  });
                  Navigator.of(context)
                      .push(new MaterialPageRoute(builder: (_) {
                    return new CartFood(counter.toString(), subttl.toString());
                  }));
                }),
            counter != 0
                ? new Positioned(
                    right: 11,
                    top: 11,
                    child: new Container(
                      padding: EdgeInsets.all(2),
                      decoration: new BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(6),
                      ),
                      constraints: BoxConstraints(
                        minWidth: 14,
                        minHeight: 14,
                      ),
                      child: Text(
                        '$counter',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 8,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                : new Container()
          ],
        ),
      );
    }
  }

  Widget _icon_list() {
    if (list == 0) {
      return Stack(
        children: <Widget>[
          new IconButton(
              icon: Icon(Icons.assignment),
              onPressed: () {
                setState(() {
                  list = 0;
                });
              }),
          list != 0
              ? new Positioned(
                  right: 11,
                  top: 11,
                  child: new Container(
                    padding: EdgeInsets.all(2),
                    decoration: new BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(6),
                    ),
                    constraints: BoxConstraints(
                      minWidth: 14,
                      minHeight: 14,
                    ),
                    child: Text(
                      '$list',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 8,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                )
              : new Container()
        ],
      );
    } else {
      return InkWell(
        onTap: () {
          Navigator.of(context).push(new MaterialPageRoute(builder: (_) {
            return new PesananSaya(list.toString(), subttl.toString(), "resto");
          }));
        },
        child: new Stack(
          children: <Widget>[
            new IconButton(
                icon: Icon(Icons.assignment),
                onPressed: () {
                  setState(() {
                    list = 0;
                  });
                  Navigator.of(context)
                      .push(new MaterialPageRoute(builder: (_) {
                    return new PesananSaya(
                        list.toString(), subttl.toString(), "resto");
                  }));
                }),
            list != 0
                ? new Positioned(
                    right: 11,
                    top: 11,
                    child: new Container(
                      padding: EdgeInsets.all(2),
                      decoration: new BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(6),
                      ),
                      constraints: BoxConstraints(
                        minWidth: 14,
                        minHeight: 14,
                      ),
                      child: Text(
                        '$list',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 8,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                : new Container()
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(new MaterialPageRoute(builder: (_) {
                  return new HomePage();
                }));
              }),
          backgroundColor: hexToColor("#344b6b"),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(children: <Widget>[Text("Restaurant")]),
              Column(children: <Widget>[
                Row(
                  children: <Widget>[
                    _icon_cart(),
                    _icon_list(),
                  ],
                ),
              ])
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) =>
                      isPunyaToko ? BarangPage() : MarketPlaceRegisterPage(),
                ));
          },
          label: Text(isPunyaToko ? 'Atur Toko' : 'Buka Toko'),
          icon: Icon(Icons.shop),
          backgroundColor: Colors.green,
        ),
        body: new Column(
          children: <Widget>[
            new Container(
              color: hexToColor("#344b6b"),
              child: new Padding(
                padding: const EdgeInsets.all(3),
                child: new Card(
                  child: new ListTile(
                    leading: new Icon(Icons.search),
                    title: new TextField(
                      controller: searchController,
                      decoration: new InputDecoration(
                          hintText: 'Search', border: InputBorder.none),
                      onChanged: (text) {
                        getData();
                      },
                    ),
                    trailing: new IconButton(
                      icon: new Icon(Icons.cancel),
                      onPressed: () {
                        searchController.clear();
                        getData();
                      },
                    ),
                  ),
                ),
              ),
            ),
            new Expanded(
                child: isLoading
                    ? loadingScreen()
                    : ListView.separated(
                        itemCount: data == null ? 0 : data.length,
                        itemBuilder: (context, index) {
                          return InkWell(
                              onTap: () {
                                Navigator.of(context)
                                    .push(new MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      new MakananDetailPage(
                                          data[index]['id'],
                                          data[index]['nama_makanan'],
                                          data[index]['harga'],
                                          data[index]['img'],
                                          data[index]['keterangan']),
                                ));
                              },
                              child: Column(
                                children: <Widget>[
                                  Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      child: Container(
                                        padding: EdgeInsets.all(10.0),
                                        child: Column(
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.all(15.0),
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10.0)),
                                                boxShadow: <BoxShadow>[
                                                  new BoxShadow(
                                                    color: Colors.black12,
                                                    blurRadius: 10.0,
                                                    offset:
                                                        new Offset(0.0, 10.0),
                                                  ),
                                                ],
                                              ),
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Flexible(
                                                    flex: 5,
                                                    child: Image.network(
                                                      data[index]['img'],
                                                      width: 80.0,
                                                      height: 80.0,
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding: EdgeInsets.only(
                                                        left: 10.0,
                                                        right: 10.0),
                                                  ),
                                                  Flexible(
                                                      flex: 5,
                                                      child: Align(
                                                        alignment:
                                                            Alignment.topRight,
                                                        child: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .end,
                                                          children: <Widget>[
                                                            Row(
                                                              children: <
                                                                  Widget>[
                                                                Container(
                                                                  margin: const EdgeInsets
                                                                          .only(
                                                                      left:
                                                                          15.0),
                                                                  child: Text(
                                                                    data[index][
                                                                        'nama_makanan'],
                                                                    style: TextStyle(
                                                                        fontSize:
                                                                            16.0,
                                                                        fontWeight:
                                                                            FontWeight.bold),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                            Padding(
                                                              padding: EdgeInsets
                                                                  .only(
                                                                      top:
                                                                          14.0),
                                                            ),
                                                            Row(
                                                              children: <
                                                                  Widget>[
                                                                Container(
                                                                  margin: const EdgeInsets
                                                                          .only(
                                                                      left:
                                                                          15.0),
                                                                  child: Text(
                                                                    thousandSeparator(data[index]
                                                                            [
                                                                            'harga']
                                                                        .toString()),
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .green,
                                                                        fontSize:
                                                                            13.0,
                                                                        fontWeight:
                                                                            FontWeight.bold),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ],
                                                        ),
                                                      )),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      )),
                                ],
                              ));
                        },
                        separatorBuilder: (context, index) {
                          return Divider();
                        },
                      )),
          ],
        ));
  }
}
