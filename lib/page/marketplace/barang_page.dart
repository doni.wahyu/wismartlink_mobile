import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/page/marketplace/barang_update_page.dart';

import 'barang_register_page.dart';

class BarangPage extends StatefulWidget {
  const BarangPage({Key key}) : super(key: key);

  @override
  _BarangPageController createState() => _BarangPageController();
}

class _BarangPageController extends State<BarangPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  bool isPunyaToko = false;
  List data;
  TextEditingController searchController = new TextEditingController();

  @override
  void initState() {
    super.initState();

    getData();
  }

  @override
  Widget build(BuildContext context) => _BarangPageView(this);

  void getData() async {
    setState(() => isLoading = true);

    try {
      Response response = await new Dio().post(
          API_URL + 'list_barang_by_toko_id',
          data: await new FormData.fromMap({
            "id_toko": await getSession("id_toko"),
            "keyword": searchController.text
          }),
          options: Options(method: 'POST', responseType: ResponseType.json));

      setState(() => isLoading = false);

      if (response.toString() != '') {
        setState(() {
          data = response.data['data'];
        });
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      print(e);
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    }
  }

  void awaitReturnRegisterBarang() async {
    final result = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => BarangRegisterPage(),
        ));

    getData();
  }
}

class _BarangPageView extends StatelessWidget {
  final _BarangPageController state;

  const _BarangPageView(this.state, {Key key}) : super(key: key);

  _delteConfirmation(barangId) {
    print('barangId ' + barangId);
    showDialog(
      context: state.scaffoldKey.currentContext,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Delete Barang",
              style: new TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 14.0)),
          content: SingleChildScrollView(
            child: Container(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Are You Sure ? ",
                      style: TextStyle(color: Colors.black),
                    )
                  ]),
            ),
          ),
          actions: <Widget>[
            new FlatButton(
              color: Colors.green,
              child: new Text(
                "Delete",
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.pop(context);
                _deleteBarang(barangId);
              },
            ),
            // usually buttons at the bottom of the dialog
            new FlatButton(
              color: Colors.red,
              child: new Text(
                "Cancel",
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                //UPDATE TRACK KE 4.1 YAITU PENGAJUAN DILAKUKAN SURVEY EKSTERNAL
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  _deleteBarang(barangId) async {
    var param = {
      "id": barangId,
    };
    print('delete ' + param.toString());
    // print('_selectedDestination ' + _selectedDestination.toString());
    final url = API_URL;
    var dio = new Dio();
    try {
      Response response;
      response = await dio.post(url + "disable_barang_toko",
          data: param,
          options: new Options(
            headers: {"Content-Type": "application/x-www-form-urlencoded"},
          ));

      print(response.data.toString());
      print(response.data['success']);
      if (response.data['success'] == true) {
        Alert(
          context: state.scaffoldKey.currentContext,
          type: AlertType.success,
          title: "Delete Barang",
          desc: response.data['message'],
          buttons: [
            DialogButton(
              child: Text(
                "OK",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () {
                Navigator.pop(state.scaffoldKey.currentContext);
                Navigator.of(state.scaffoldKey.currentContext)
                    .pushReplacement(new MaterialPageRoute(builder: (_) {
                  return new BarangPage();
                }));
              },
              width: 120,
            )
          ],
        ).show();
      } else {
        Alert(
          context: state.scaffoldKey.currentContext,
          type: AlertType.error,
          title: "Delete Barang",
          desc: response.data['message'],
          buttons: [
            DialogButton(
              child: Text(
                "OK",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () {
                Navigator.pop(state.scaffoldKey.currentContext);
              },
              width: 120,
            )
          ],
        ).show();
      }
    } on DioError catch (e) {
      debugPrint("Error $e");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: state.scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(children: <Widget>[Text("Barang")]),
              Column(children: <Widget>[
                InkWell(
                    onTap: () {
                      Navigator.push(
                          state.context,
                          MaterialPageRoute(
//                            builder: (context) => NotifikasiPage(),
                              ));
                    },
                    child: Icon(
                      Icons.shopping_cart,
                      color: Colors.white,
                    ))
              ])
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () => state.awaitReturnRegisterBarang(),
          label: Text('Tambah Barang'),
          icon: Icon(Icons.add_to_photos),
          backgroundColor: Colors.green,
        ),
        body: new Column(
          children: <Widget>[
            new Container(
              color: hexToColor("#344b6b"),
              child: new Padding(
                padding: const EdgeInsets.all(3),
                child: new Card(
                  child: new ListTile(
                    leading: new Icon(Icons.search),
                    title: new TextField(
                      controller: state.searchController,
                      decoration: new InputDecoration(
                          hintText: 'Search', border: InputBorder.none),
                      onChanged: (text) {
                        state.getData();
                      },
                    ),
                    trailing: new IconButton(
                      icon: new Icon(Icons.cancel),
                      onPressed: () {
                        state.searchController.clear();
                        state.getData();
                      },
                    ),
                  ),
                ),
              ),
            ),
            new Expanded(
                child: state.isLoading
                    ? loadingScreen()
                    : ListView.separated(
                        itemCount: state.data == null ? 0 : state.data.length,
                        itemBuilder: (context, index) {
                          return Row(
                            children: <Widget>[
                              Expanded(
                                  flex: 2,
                                  child: Image.network(
                                    state.data[index]['img'],
                                    width: 80.0,
                                    height: 80.0,
                                  )),
                              Expanded(
                                flex: 2,
                                child: Column(
                                  children: <Widget>[
                                    Align(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                            state.data[index]['nama_barang'])),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(thousandSeparator(
                                          state.data[index]['harga']), style: TextStyle(color: Colors.grey),),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: InkWell(
                                    onTap: () {
                                      Navigator.of(context).pushReplacement(
                                          new MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            new BarangUpdatePage(
                                                state.data[index]['id'], 
                                                state.data[index]
                                                    ['nama_barang'],
                                                state.data[index]['harga'],
                                                state.data[index]['keterangan'],
                                                state.data[index]['img']),
                                      ));
                                    },
                                    child: Icon(Icons.edit)),
                              ),
                              Expanded(
                                flex: 2,
                                child: InkWell(
                                  onTap: () {
                                    _delteConfirmation(state.data[index]['id']);
                                    // _deleteDestination(dest[i].destinationId);
                                  },
                                  child: Icon(
                                    Icons.delete,
                                    color: Colors.black,
                                    size: 20.0,
                                  ),
                                ),
                              )
                            ],
                          );
                        },
                        separatorBuilder: (context, index) {
                          return Divider();
                        },
                      )),
          ],
        ));
  }
}
